import { Document, FilterQuery } from 'mongoose'

declare module 'mongoose' {
	interface Model<T extends Document<any>> {
		smartQuery( query:{ [key:string]:string | string[] } ):Promise<Array<any>>;
		smartCount( query:{ [key:string]:string | string[] } ):Promise<number>;
		getOne ( filter:FilterQuery<T> ):Promise<T>;
	}
}
