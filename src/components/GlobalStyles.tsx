import { createStyles, makeStyles, colors } from '@material-ui/core'

const useStyles = makeStyles( () => createStyles( {
	'@global': {
		'*': {
			boxSizing: 'border-box',
			margin: 0,
			padding: 0
		},
		html: {
			'-webkit-font-smoothing': 'antialiased',
			'-moz-osx-font-smoothing': 'grayscale',
			height: '100%',
			width: '100%'
		},
		'::-webkit-scrollbar': {
			width: '8px',
			height: '8px'
		},
		'::-webkit-scrollbar-thumb': {
			background: colors.grey[300],
			borderRadius: '4px'
		},
		'::-webkit-scrollbar-thumb:active': {
			background: '#FFFFFF'
		},
		'::-webkit-scrollbar-thumb:hover': {
			background: colors.grey[400],
			width: '16px'
		},
		'::-webkit-track': {
			background: '#e1e1e1',
			borderRadius: '4px'
		},
		'::-webkit-scrollbar-track:hover,::-webkit-scrollbar-track:active': {
			background: colors.grey[100],
			width: '16px',
			borderRadius: '4px'
		},
		body: {
			backgroundColor: '#f4f6f8',
			height: '100%',
			width: '100%'
		},
		a: {
			textDecoration: 'none'
		},
		'#root': {
			height: '100%',
			width: '100%'
		}
	}
} ) )

const GlobalStyles = () => {
	useStyles()

	return null
}

export default GlobalStyles
