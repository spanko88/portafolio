/* eslint-disable react/prop-types */
import { FC } from 'react'
import { TextField, Grid, IconButton, Box, colors } from '@material-ui/core'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import clsx from 'clsx'

import { Field, FieldArray, FieldProps } from 'formik'

import AddIcon from '@material-ui/icons/AddCircle'
import IndeterminateCheckBoxIcon from '@material-ui/icons/RemoveCircle'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	field: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'flex-start'
	},
	space: {
		// flexGrow: 1,
		marginLeft: '8px'
	},
	error: {
		height: '16px',
		width: '100%',
		padding: 0,
		display: 'flex',
		justifyContent: 'flex-end'
	},
	flex: {
		display: 'flex',
		justifyContent: 'space-between'
	},
	arrayTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	arrayItem: {
		[theme.breakpoints.down( 'md' )]: {
			borderBottom: `solid 1px ${colors.grey[400]}`,
			marginBottom: '24px'
		}
	}
} ) )

interface IMuiFormikArrayProps {
	label:string[];
	name:string;
}

const MuiFormikArrayObj:FC<IMuiFormikArrayProps> = ( { label, name } ) => {
	const classes = useStyles()
	return (
		<FieldArray name={ name }>
			{
				( fieldArrayProps:any ) => {
					const items = fieldArrayProps.form.values[fieldArrayProps.name]
					return (
						<>
							<Box sx={ { position: 'absolute', left: '0px', top: '-16px', backgroundColor: '#fff', zIndex: 2000 } }>
								<IconButton size='small' onClick={ () => { fieldArrayProps.unshift( { phone: '', description: '' } ) } }>
									<AddIcon color='primary' />
								</IconButton>
							</Box>
							{
								items && items.map( ( item, i:number ) => {
									// console.log( item )
									return (
										<Box key={ `${fieldArrayProps.name}${i}` } className={ clsx( { [classes.arrayItem]: i < items.length - 1 } ) }>
											{
												items.length > 1 && (
													<Box sx={ { display: 'flex', justifyContent: 'flex-end', margin: '0 0 8px 0' } }>
														<IconButton size='small' onClick={ () => { fieldArrayProps.remove( i ) } }>
															<IndeterminateCheckBoxIcon color='secondary' />
														</IconButton>
													</Box>
												)
											}
											<Grid container spacing={ 1 }>
												<Field name={ `${fieldArrayProps.name}[${i}].phone` }>
													{
														( fieldProps:FieldProps ) => {
														// console.log( fieldProps.meta )
															return (
																<>
																	<Grid item xs={ 12 } md={ 3 }>
																		<TextField
																			fullWidth
																			// disabled={ i < ( items.length - 1 ) }
																			size='small'
																			variant='outlined'
																			autoComplete='off'
																			name={ fieldProps.field.name }
																			label={ `${label[0]} ${i + 1}` }
																			helperText = {
																				fieldProps.meta.touched && fieldProps.meta.error
																					? <span className={ classes.error }>{fieldProps.meta.touched && fieldProps.meta.error}</span>
																					: <span></span>
																			}
																			error={ Boolean( fieldProps.meta.touched && fieldProps.meta.error ) }
																			onChange={ fieldProps.field.onChange }
																			onBlur={ fieldProps.field.onBlur }
																			value={ fieldProps.field.value }

																		/>
																	</Grid>
																</> )
														}
													}
												</Field>
												<Field name={ `${fieldArrayProps.name}[${i}].description` }>
													{
														( fieldProps:FieldProps ) => {
														// console.log( fieldProps.field.name, fieldProps.meta.error )
															return (
																<>
																	<Grid item xs={ 12 } md={ 9 }>
																		<TextField
																			fullWidth
																			multiline
																			minRows={ 1 }
																			inputProps={ { style: { minHeight: '16px' } } }
																			size='small'
																			variant='outlined'
																			name={ fieldProps.field.name }
																			label={ `${label[1]} ${i + 1}` }
																			helperText = {
																				fieldProps.meta.touched && fieldProps.meta.error
																					? <span className={ classes.error }>{fieldProps.meta.touched && fieldProps.meta.error}</span>
																					: <span>Opcional</span>
																			}
																			error={ Boolean( fieldProps.meta.touched && fieldProps.meta.error ) }
																			onChange={ fieldProps.field.onChange }
																			onBlur={ fieldProps.field.onBlur }
																			value={ fieldProps.field.value }
																		/>
																	</Grid>
																</> )
														}
													}
												</Field>
											</Grid>
										</Box>
									)
								} )
							}
						</>
					)
				}
			}
		</FieldArray>
	)
}

export default MuiFormikArrayObj
