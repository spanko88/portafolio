/* eslint-disable react/prop-types */
import { FC } from 'react'
import { Field, useField } from 'formik'
import { TextField } from '@material-ui/core'
import { makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles( ( theme:Theme ) => ( {
	error: {
		height: '16px',
		width: '100%',
		padding: 0,
		display: 'flex',
		justifyContent: 'flex-end'
	}
} ) )

interface IMuiFormikInputProps {
	label:string;
	name:string;
	type?:string;
	helperText?:string;
	multiline?:boolean,
	[x:string]:any
}

const MuiFormikInput:FC<IMuiFormikInputProps> = ( { label, name, type = 'text', helperText, multiline = false, ...props } ) => {
	const classes = useStyles()
	const [field, meta] = useField( { name } )

	return (
		<Field
			as='div'
			name={ name }
			label={ label }
		>
			<TextField
				autoComplete='off'
				multiline={ multiline }
				size='small'
				fullWidth
				variant='outlined'
				name={ name }
				label={ label }
				type={ type }
				inputProps={ { ...props } }
				helperText = {
					meta.touched && meta.error
						? <span className={ classes.error }>{meta.touched && meta.error}</span>
						: <span>{helperText}</span>
				}
				error={ Boolean( meta.touched && meta.error ) }
				value={ field.value }
			/>
		</Field>
	)
}

export default MuiFormikInput
