import { ReactNode, FC, Fragment } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	container: {
		position: 'relative',
		borderTop: 'solid 1px lightgrey',
		borderColor: theme.palette.primary.main,
		padding: '24px 10px 6px',
		marginBottom: '16px',
		marginTop: '16px'
	},
	title: {
		position: 'absolute',
		top: '-13px',
		left: '35px',
		background: theme.palette.background.default,
		color: theme.palette.primary.main,
		fontWeight: 500,
		padding: '0 4px',
		zIndex: 100
	}
} ) )

interface IMuiFormContainer {
	children:ReactNode;
	label?:string;
	color?:string
}

const MuiFormContainer:FC<IMuiFormContainer> = ( { children, label = null } ) => {
	const classes = useStyles( )
	return (
		<Fragment>
			<div className={ classes.container }>
				{label && <Typography component='span' variant='h6' className={ classes.title }>{label}</Typography>}
				{children}
			</div>
		</Fragment>
	)
}

export default MuiFormContainer
