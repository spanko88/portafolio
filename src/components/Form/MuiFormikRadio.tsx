/* eslint-disable react/prop-types */
import { FC, ReactNode } from 'react'
import { Field, ErrorMessage, useField, FieldInputProps } from 'formik'
import { FormControl, FormControlLabel, Radio, RadioGroup, FormLabel } from '@material-ui/core'
import { makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles( ( theme:Theme ) => ( {
	root: {
		display: 'flex',
		justifyContent: 'flex-start',
		alignItems: 'center',
		flexDirection: 'column'
	},
	error: {
		height: '20px',
		display: 'flex',
		justifyContent: 'flex-end'
	},
	radio: {
		height: '32px'
	}
} ) )

interface IFormikRadioFieldProps extends FieldInputProps<string> {
	children:ReactNode;
	label:string;
	helperText?:string;
	error:boolean;
	defaultValue:string;
}

const FormikRadioField:FC<IFormikRadioFieldProps> = ( { children, label, helperText, error, value, name, defaultValue, onChange } ) => {
	return (
		<FormControl error={ error } margin='dense'>
			<FormLabel component="legend">{label}</FormLabel>
			<RadioGroup aria-label={ label } name={ name } onChange={ onChange } value={ value || defaultValue }>
				{children}
			</RadioGroup>
			{/* <FormHelperText>{helperText}</FormHelperText> */}
		</FormControl>
	)
}

interface IRadioItems {
	label:string,
	value:string
}

interface IMuiFormikRadioProps {
	label:string;
	name:string;
	items:IRadioItems[]
}

const MuiFormikRadio:FC<IMuiFormikRadioProps> = ( { label, name, items } ) => {
	const classes = useStyles()
	const [, meta] = useField( { name } )
	return (
		<Field
			name={ name }
			as={ FormikRadioField }
			label={ label }
			helperText = { <span className={ classes.error }><ErrorMessage name={ name } /></span> }
			error={ Boolean( meta.touched && meta.error ) }
			defaultValue={ items[0].value }
		>
			{
				items.map( ( item, i ) => (
					<FormControlLabel key={ item.value + i } value={ item.value } label={ item.label } control={ <Radio color='primary' /> } className={ classes.radio } />
				) )
			}
		</Field>
	)
}

export default MuiFormikRadio
