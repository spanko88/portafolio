/* eslint-disable react/prop-types */
import { FC, ReactNode } from 'react'
import { Field, ErrorMessage, useField, FieldInputProps } from 'formik'
import { Grid, Select, FormControl, FormHelperText, MenuItem, InputLabel } from '@material-ui/core'
import { makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles( ( theme:Theme ) => ( {
	error: {
		// height: '20px',
		display: 'flex',
		justifyContent: 'flex-end'
	},
	formControl: {
		margin: theme.spacing( 1 ),
		minWidth: 120
	},
	selectEmpty: {
		marginTop: theme.spacing( 2 )
	}
} ) )

interface IFormikSelectFieldProps extends FieldInputProps<string> {
	children:ReactNode;
	label:string;
	helperText?:string;
	error:boolean;
}

const FormikSelectField:FC<IFormikSelectFieldProps> = ( { children, label, helperText, error, value, name, onBlur, onChange } ) => {
	return (
		<FormControl variant='outlined' fullWidth size="small" error={ error }>
			<InputLabel>{label}</InputLabel>
			<Select label={ label } name={ name } onChange={ onChange } onBlur={ onBlur } value={ value }>
				{children}
			</Select>
			<FormHelperText>{helperText}</FormHelperText>
		</FormControl>
	)
}

interface ISelectItems {
	label:string,
	value:string | number,
}

interface IMuiFormikSelectProps {
	label:string;
	name:string;
	items:ISelectItems[]
}

const MuiFormikSelect:FC<IMuiFormikSelectProps> = ( { label, name, items } ) => {
	const classes = useStyles()
	const [, meta] = useField( { name } )
	return (
		<Grid item xs={ 12 }>
			<Field
				name={ name }
				as={ FormikSelectField }
				label={ label }
				helperText = { <span className={ classes.error }><ErrorMessage name={ name } /></span> }
				error={ Boolean( meta.touched && meta.error ) }
			>
				<MenuItem value='' disabled>Ninguno</MenuItem>
				{
					items?.length > 0 && items.map( ( item, i ) => (
						<MenuItem key={ `${item.value}_${i}` } value={ item.value }>
							{item.label}
						</MenuItem>
					) )
				}
			</Field>
		</Grid>
	)
}

export default MuiFormikSelect
