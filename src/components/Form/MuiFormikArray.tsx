/* eslint-disable react/prop-types */
import { FC } from 'react'
import { TextField, InputAdornment } from '@material-ui/core'
import { makeStyles, Theme } from '@material-ui/core/styles'

import { Field, FieldArray, FieldProps } from 'formik'

import AddBoxIcon from '@material-ui/icons/AddBox'
import IndeterminateCheckBoxIcon from '@material-ui/icons/IndeterminateCheckBox'

const useStyles = makeStyles( ( theme:Theme ) => ( {
	field: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'flex-start'
	},
	space: {
		// flexGrow: 1,
		marginLeft: '8px'
	},
	error: {
		height: '5px',
		width: '100%',
		padding: 0,
		display: 'flex',
		justifyContent: 'flex-end'
	}
} ) )

interface IMuiFormikArrayProps {
	label:string;
	name:string;
}

const MuiFormikArray:FC<IMuiFormikArrayProps> = ( { label, name } ) => {
	const classes = useStyles()
	return (
		<FieldArray name={ name }>
			{
				( fieldArrayProps ) => {
					// console.log( fieldArrayProps )
					const items = fieldArrayProps.form.values[fieldArrayProps.name]
					return (
						<>
							{
								items.map( ( item, i:number ) => {
									// console.log( item )
									return (
										<Field name={ `${fieldArrayProps.name}[${i}]` } key={ `${fieldArrayProps.name}${i}` }>
											{
												( fieldProps:FieldProps ) => {
													// console.log( fieldProps.field.name, fieldProps.meta.error )
													return ( <>
														<TextField
															fullWidth
															// disabled={ i < ( items.length - 1 ) }
															size='small'
															variant='outlined'
															autoComplete='off'
															name={ fieldProps.field.name }
															label={ `${label} ${i + 1}` }
															helperText = { <span className={ classes.error }>{fieldProps.meta.touched && fieldProps.meta.error}</span> }
															error={ Boolean( fieldProps.meta.touched && fieldProps.meta.error ) }
															onChange={ fieldProps.field.onChange }
															onBlur={ fieldProps.field.onBlur }
															value={ fieldProps.field.value }
															InputProps={
																{
																	endAdornment: <InputAdornment position="end">
																		{
																			i > 0 && (
																				<IndeterminateCheckBoxIcon color={ fieldProps.meta.error !== undefined ? ( 'secondary' ) : ( 'disabled' ) } onClick={
																					() => {
																						fieldArrayProps.remove( i )
																					}
																				} />
																			)
																		}
																		{
																			i === ( items.length - 1 ) && fieldProps.meta.error === undefined && (
																				<AddBoxIcon color='primary' onClick={
																					() => {
																						fieldArrayProps.push( '' )
																					}
																				} />
																			)
																		}
																	</InputAdornment>
																}
															}
														/>
													</> )
												}
											}
										</Field>
									)
								} )
							}
						</>
					)
				}
			}
		</FieldArray>
	)
}

export default MuiFormikArray
