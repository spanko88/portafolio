/* eslint-disable react/prop-types */
import { FC } from 'react'
import { TextField, Autocomplete } from '@material-ui/core'
import { makeStyles, Theme } from '@material-ui/core/styles'

import { Field, FieldProps } from 'formik'

const useStyles = makeStyles( ( theme:Theme ) => ( {
	error: {
		width: '100%',
		padding: 0,
		display: 'flex',
		justifyContent: 'flex-end'
	},
	row: {
		border: 'solid 1px lightgrey'
	}
} ) )
interface IMuiFormikACProps {
	label:string;
	name:string;
	options:string[]
}

const MuiFormikAC:FC<IMuiFormikACProps> = ( { label, name, options } ) => {
	const classes = useStyles()
	// console.log( options )
	return (
		<Field name={ name }>
			{
				( fieldProps:FieldProps ) => {
					// console.log( fieldProps.form )
					return (
						<>
							{
								options && <Autocomplete
									fullWidth
									noOptionsText='Sin resultado'
									size='small'
									options={ options }
									renderInput={
										( params ) => (
											<TextField { ...params }
												label={ label }
												autoComplete='off'
												size='small'
												variant="outlined"
												name={ fieldProps.field.name }
												onBlur={ fieldProps.field.onBlur }
												helperText = { <span className={ classes.error }>{fieldProps.meta.touched && fieldProps.meta.error}</span> }
												error={ Boolean( fieldProps.meta.touched && fieldProps.meta.error ) }
											/>
										)
									}
									onChange={ ( e, value ) => fieldProps.form.setFieldValue( name, value ) }
									value={ fieldProps.field.value }
								/>
							}
						</>
					)
				}
			}
		</Field>
	)
}

export default MuiFormikAC
