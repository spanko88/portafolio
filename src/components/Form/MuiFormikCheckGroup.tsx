import { FC, Fragment } from 'react'
import { Field } from 'formik'
import { Checkbox } from '@material-ui/core'

interface IProps {
	options?:any[],
	name?:string
}
const MuiFormikCheckGroup:FC<IProps> = ( { name, options } ) => {
	return (
		<Field name={ 'justice' }>
			{
				( props ) => {
					console.log( props )
					return (
						<Fragment>
							<Checkbox name={ props.name } />
							<Checkbox name={ props.name } />
							<Checkbox name={ props.name } />
							<Checkbox name={ props.name } />
						</Fragment>
					)
				}
			}
		</Field>
	)
}

export default MuiFormikCheckGroup
