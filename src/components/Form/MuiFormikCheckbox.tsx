/* eslint-disable react/prop-types */
import { FC } from 'react'
import { Field, useField, FieldInputProps } from 'formik'
import { Grid, FormControl, FormHelperText, Checkbox } from '@material-ui/core'
import { makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles( ( theme:Theme ) => ( {
	root: {
		// display: 'flex',
		// justifyContent: 'center',
		// alignItems: 'center',
		// flexDirection: 'column'
	},
	control: {
		display: 'flex',
		justifyContent: 'flex-start'
	},
	error: {
		margin: 0,
		alignContent: 'center',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	span: {
		textAlign: 'left'
	},
	radio: {
		height: '32px'
	}
} ) )

interface IFormikCheckboxFieldProps extends FieldInputProps<boolean> {
	label:string;
	error:boolean;
	helpers:any
}

const FormikCheckboxField:FC<IFormikCheckboxFieldProps> = ( { label, error, value, name, onChange, helpers } ) => {
	const classes2 = useStyles()
	return (
		<>
			<FormControl variant='outlined' fullWidth margin="dense" error={ error }>
				<div className={ classes2.control }>
					<Checkbox color='primary' checked={ value } onChange={ onChange } name={ name } inputProps={ { 'aria-label': name } } onClick={ e => { helpers.setTouched( true ) } } />
					<FormHelperText className={ classes2.error }><span className={ classes2.span }>{label}</span></FormHelperText>
				</div>
			</FormControl>
		</>
	)
}

interface IMuiFormikCheckboxProps {
	label:string;
	name:string;
}

const MuiFormikCheckbox:FC<IMuiFormikCheckboxProps> = ( { label, name } ) => {
	const classes = useStyles()
	const [, meta, helpers] = useField( { name } )

	return (
		<Grid item className={ classes.root }>
			<Field
				name={ name }
				as={ FormikCheckboxField }
				label={ label }
				error={ Boolean( meta.touched && meta.error ) }
				helpers={ helpers }
			/>
		</Grid>
	)
}

export default MuiFormikCheckbox
