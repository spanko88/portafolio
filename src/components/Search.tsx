import { useState, useCallback, useEffect, useRef } from 'react'
import { colors, Typography, TextField, CircularProgress, InputAdornment, IconButton } from '@material-ui/core'
import { makeStyles, Theme } from '@material-ui/core/styles'
import clsx from 'clsx'
import { debounce } from 'lodash'
import DeleteIcon from '@material-ui/icons/Clear'
import DoneIcon from '@material-ui/icons/CheckCircleOutline'
import { apiAdmin } from '~/axios'

const useStyles = makeStyles( ( theme:Theme ) => ( {
	searchContainer: {
		position: 'relative'
	},
	searchOptionsContainer: {
		position: 'absolute',
		backgroundColor: '#fff',
		zIndex: 100,
		transform: 'translate(0,0px)',
		width: '100%',
		border: `solid 0.5px ${colors.grey[200]}`,
		borderRadius: '5px',
		marginTop: '1px',
		textOverflow: 'ellipsis',
		boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)'
	},
	searchOptions: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		padding: '6px 15px',
		cursor: 'pointer',
		'&:hover': {
			backgroundColor: colors.grey[100]
		}
	},
	textOverflow: {
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis'
	},
	searchProgress: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	}
} ) )

const Search = ( { handleSelection, handleChangePage, queries, url } ) => {
	const classes = useStyles()
	const [searchInput, setSearchInput] = useState( '' )
	const [searchOptions, setSearchOptions] = useState( [] )
	const [isLoadingSearch, setIsLoadingSearch] = useState( false )
	const [isSearchOptionsOpen, setIsSearchOptionsOpen] = useState( false )
	const [isSelected, setIsSelected] = useState( '' )
	const wrapperRef = useRef( null )

	const debounceSearch = useCallback( debounce( async ( value ) => {
		setIsLoadingSearch( true )
		try {
			const { data } = await apiAdmin.get( url, {
				params: {
					$q: value,
					$limit: 5,
					...queries
				}
			} )
			setSearchOptions( data.data )
			setIsLoadingSearch( false )
		} catch ( error ) {
			setSearchOptions( [] )
			setIsLoadingSearch( false )
		}
	}, 500 ), [queries] )

	const handleOnChangeSearchInput = ( e ) => {
		const { target: { value } } = e
		setSearchInput( value )
		setIsSearchOptionsOpen( true )
		if ( value === '' ) return setSearchOptions( [] )
		debounceSearch( value )
	}

	const handleSearchOptionSelect = ( item ) => {
		setIsSearchOptionsOpen( false )
		handleChangePage( null, 0 )
	}

	const handleClearIconSearchInput = () => {
		setSearchInput( '' )
		handleSelection( '' )
		setSearchOptions( [] )
		setIsSearchOptionsOpen( false )
		handleChangePage( null, 0 )
	}

	const handleDoneIconSearchInput = () => {
		handleSelection( searchInput )
		setIsSelected( searchInput )
		// setSearchOptions( [] )
		setIsSearchOptionsOpen( false )
		handleChangePage( null, 0 )
	}

	const handleSearchInputKeyPress = ( e ) => {
		const { target: { value }, key } = e
		if ( key === 'Enter' ) {
			handleSelection( value )
			setIsSelected( value )
			setIsSearchOptionsOpen( false )
			handleChangePage( null, 0 )
		}
	}

	const handleClickOutside = ( e ) => {
		const { current: wrap } = wrapperRef
		if ( wrap && !wrap.contains( e.target ) ) {
			setIsSearchOptionsOpen( false )
			// setSearchOptions( [] )
		}
	}

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside )
		return () => {
			document.removeEventListener( 'mousedown', handleClickOutside )
		}
	}, [] )

	return (

		<div className={ classes.searchContainer } ref={ wrapperRef }>
			<TextField
				fullWidth
				label='Buscar'
				variant='outlined'
				size='small'
				name='searchInput'
				onChange={ handleOnChangeSearchInput }
				value={ searchInput }
				onKeyPress={ handleSearchInputKeyPress }
				InputProps={
					{
						autoComplete: 'off',
						endAdornment: <InputAdornment position="end">
							{
								searchInput &&
								(
									<>
										{
											isLoadingSearch
												? (
													<div className={ clsx( classes.searchProgress, classes.searchOptions ) }>
														<CircularProgress disableShrink size={ 20 } style={ { color: 'grey' } } />
													</div>
												)
												: (
													<>
														<IconButton size='small' onClick={ handleClearIconSearchInput }>
															<DeleteIcon style={ { color: 'lightgrey' } } />
														</IconButton>
														{
															isSelected !== searchInput && ( <IconButton size='small' onClick={ handleDoneIconSearchInput }>
																<DoneIcon style={ { color: 'lightgreen' } } />
															</IconButton> )
														}
													</>
												)
										}
									</>
								)
							}
						</InputAdornment>,
						inputProps: {
							onClick: () => setIsSearchOptionsOpen( true )
						}
					}
				}
			/>
			{
				isSearchOptionsOpen && searchInput.length > 0 &&
				(
					<div className={ classes.searchOptionsContainer } tabIndex={ 0 }>
						{
							isLoadingSearch
								? null
								: ( searchOptions.length <= 0
									? (
										<div className={ classes.searchOptions }>
											<Typography>
											Sin coincidencias
											</Typography>
										</div>
									)
									: ( searchOptions.map( ( item, i ) => (
										<div
											key={ 'item_key' + i }
											className={ clsx( classes.searchOptions ) }
											onClick={ () => { handleSearchOptionSelect( item ) } }
											tabIndex={ 0 }
										>
											<Typography variant='body1'>
												<strong>{item.nombres}</strong>
											</Typography>
											<Typography variant='h6' className={ classes.textOverflow }>
												{item.nombres}
											</Typography>
										</div> )
									) )
								)
						}
					</div>
				)
			}
		</div>
	)
}

export default Search
