import { FC } from 'react'
import { Modal, Box } from '@material-ui/core'
import Loading from './components/Loading'

interface IComponent {
	isLoading:boolean,
	handleClose:()=>void
}

const Component:FC<IComponent> = ( { isLoading, handleClose } ) => {
	return (
		<>
			<Modal
				open={ isLoading }
				onClose={ handleClose }
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box sx={ { display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100vw', height: '100vh' } }>
					<Loading />
				</Box>
			</Modal>
		</>
	)
}

export default Component
