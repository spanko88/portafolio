import { motion, Variants, Transition } from 'framer-motion'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'

const useStyle = makeStyles( ( theme:Theme ) => createStyles( {
	loadingContainer: {
		width: '6rem',
		height: '6rem',
		display: 'flex',
		justifyContent: 'space-evenly',
		alignItems: 'center'
		// border: '1px solid black'
	},
	loadingCircle: {
		display: 'block',
		width: '10px',
		height: '10px',
		backgroundColor: theme.palette.primary.light,
		borderRadius: '20px'
	}
} ) )

const loadingContainerVariant:Variants = {
	start: {
		transition: {
			staggerChildren: 0
		}
	},
	end: {
		transition: {
			staggerChildren: 0.25
		}
	}
}

const loadingCircleVariant:Variants = {
	start: {
		y: '-30%'
	},
	end: {
		y: '30%'
	}
}

const loadingCircleTransition:Transition = {
	duration: 0.5,
	repeat: Infinity,
	repeatType: 'reverse',
	ease: 'easeInOut'
}

const Component = () => {
	const classes = useStyle()
	return (
		<motion.div
			className={ classes.loadingContainer }
			variants={ loadingContainerVariant }
			initial='start'
			animate='end'
		>
			<motion.span
				className={ classes.loadingCircle }
				variants={ loadingCircleVariant }
				transition={ loadingCircleTransition }
			/>
			<motion.span
				className={ classes.loadingCircle }
				variants={ loadingCircleVariant }
				transition={ loadingCircleTransition }
			/>
			<motion.span
				className={ classes.loadingCircle }
				variants={ loadingCircleVariant }
				transition={ loadingCircleTransition }
			/>
		</motion.div>
	)
}

export default Component
