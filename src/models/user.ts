
import mongoose from 'mongoose'
import mongooseSmartQuery from 'mongoose-smart-query'
const Schema = mongoose.Schema

const aportesSchema = new Schema( {
	description: {
		type: String
	},
	value: {
		type: String
	},
	confirm: {
		type: Boolean,
		default: false
	},
	created: {
		type: Date
	}
}, { _id: true } )

const modelName = 'User'
const userSchema = new Schema( {
	apellidos: {
		type: String,
		required: [true, 'requerido']
	},
	nombres: {
		type: String,
		required: [true, 'requerido']
	},
	cedula: {
		type: String,
		unique: true,
		required: [true, 'requerido']
	},
	nacimiento: {
		type: String,
		required: [true, 'requerido']
	},
	correo: {
		type: String,
		unique: true,
		required: [true, 'requerido']
	},
	direccion: {
		type: String,
		required: [true, 'requerido']
	},
	telefono: {
		type: String,
		required: [true, 'requerido']
	},
	nacionalidad: {
		type: String,
		required: [true, 'requerido']
	},
	sangre: {
		type: String,
		required: [true, 'requerido']
	},
	seguro: {
		type: String
	},
	nombreemergencia: {
		type: String,
		required: [true, 'requerido']
	},
	telefonoemergencia: {
		type: String,
		required: [true, 'requerido']
	},
	deporte: {
		type: String
	},
	deportecual: {
		type: String
	},
	motivacion: {
		type: String
	},
	palabraidentifica: {
		type: String
	},
	acepto: {
		type: Boolean,
		required: [true, 'requerido'],
		validate: {
			validator: ( value:boolean ) => value,
			message: 'Debe ser verdadera'
		}
	},
	rol: {
		type: String,
		default: 'user'
	},
	status: {
		type: String,
		default: 'active'
	},
	aportes: [aportesSchema]
}, { timestamps: true, validateBeforeSave: true } )

userSchema.virtual( 'completeName' ).get( function () {
	return this.nombres + ' ' + this.apellidos
} )

userSchema.plugin( mongooseSmartQuery, {
	fieldsForDefaultQuery: 'nombres apellidos cedula',
	protectedFields: 'password',
	getAllFieldsByDefault: true
} )

export default mongoose.models[modelName] || mongoose.model( modelName, userSchema, 'users' )
