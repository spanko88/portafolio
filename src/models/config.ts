import mongoose from 'mongoose'
const Schema = mongoose.Schema
const modelName = 'Config'
const configSchema = new Schema( {

} )

export default mongoose.models[modelName] || mongoose.model( modelName, configSchema, 'configs' )
