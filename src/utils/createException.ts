function exception ( msg:string, code:number ) {
	this.message = msg
	this.code = code
}

export default exception
