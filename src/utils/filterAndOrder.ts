// filtra un objeto con los valoras requeridos y ordena sus keys segun el orden
// del campo target

const filterAndOrderObject = ( obj:{[key:string]:any}, target:string[] ) => {
	const match = target.map( ( item, i ) => {
		if ( !obj[item] ) return null
		return [item, obj[item]]
	} )
	const filterNulls = match.filter( ( item, i ) => {
		return item
	} )

	const endObj:{[key:string]:any} = {}
	for ( const key of filterNulls ) {
		endObj[key[0]] = key[1]
	}
	return endObj
}

export default filterAndOrderObject

// orderFields?:string[], omitFields?:string[]
// const filteredArray = data.map( ( item ) => {
// if ( !omitFields ) return item
// return omit( item, omitFields ) //lodash omit
// } )

// const originalKeysOrder = Object.keys( filteredArray[0] )
// const keysFilteredRest = originalKeysOrder.filter( ( item ) => {
// const match = orderFields.indexOf( item )
// return match === -1
// } )
// const totalArray = orderFields.concat( keysFilteredRest )

// const ordered = filteredArray.map( ( item ) => {
// return filterAndOrderObject( item, totalArray )
// } )
// console.log( ordered )
