import XLSX from 'xlsx'
import { saveAs } from 'file-saver'
import moment from 'moment'

const downloadJsonArrayToExcel = async ( data:any[], fileName:string ) => {
	function s2ab ( s ) {
		const buf = new ArrayBuffer( s.length )
		const view = new Uint8Array( buf )
		for ( let i = 0; i < s.length; i++ ) view[i] = s.charCodeAt( i ) & 0xFF
		return buf
	}

	const wb = XLSX.utils.book_new()
	wb.Props = {
		Title: 'Descargas',
		Subject: 'descarga',
		Author: 'erik rodriguez',
		CreatedDate: new Date()
	}
	wb.SheetNames.push( fileName )

	const ws = XLSX.utils.json_to_sheet( data )
	wb.Sheets[fileName] = ws
	const wboutRes = await XLSX.write( wb, { bookType: 'xlsx', type: 'binary' } )
	saveAs( new Blob( [s2ab( wboutRes )], { type: 'application/octet-stream' } ), `${fileName}_${moment().format( 'DDMMYYYY_HHmmss' )}.xlsx` )
}

export default downloadJsonArrayToExcel
