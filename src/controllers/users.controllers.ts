import { NextApiRequest } from 'next'
import User from '~/models/user'
import { Types } from 'mongoose'
import createHttpError from 'http-errors'

export const getAll = async ( req:NextApiRequest|any ) => {
	const data:any = await User.smartQuery( req.query )
	return {
		page: req.query.$page || '1',
		total: await User.smartCount( req.query ),
		data
	}
}

export const getOne = async ( req:NextApiRequest| any, id:any ) => {
	const isValid = Types.ObjectId.isValid( id )
	if ( !isValid ) throw new createHttpError.BadRequest( `no existe usuario ${id}` )
	const data:any = User.findById( id ).select( '-password' ).lean()
	return data
}

export const createOne = async ( req:NextApiRequest|any, id:any ) => {
	const data:any = User.findById( id ).select( '-password' )
	return data
}

export const updateOne = async ( req:NextApiRequest|any, id:any ) => {
	const isValid = Types.ObjectId.isValid( id )
	if ( !isValid ) throw new createHttpError.BadRequest( `no existe usuario ${id}` )
	const query = { $set: req.body }
	const data:any = User.updateOne( { _id: id }, query )
	return data
}

export const deleteOne = async ( req:NextApiRequest|any, id:any ) => {

}
