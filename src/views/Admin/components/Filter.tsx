import { FC, useMemo } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Collapse, Paper, TextField, Grid, Box, Typography, MenuItem, Autocomplete, Button } from '@material-ui/core'

import countries from '~/utils/countries'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	container: {
		padding: '16px 16px'
	}
} ) )

const tiposSangre = [
	{ label: 'O RH+', value: 'O RH+' },
	{ label: 'O RH-', value: 'O RH-' },
	{ label: 'A RH+', value: 'A RH+' },
	{ label: 'A RH-', value: 'A RH-' },
	{ label: 'B RH+', value: 'B RH+' },
	{ label: 'B RH-', value: 'B RH-' },
	{ label: 'AB RH+', value: 'AB RH+' },
	{ label: 'AB RH-', value: 'AB RH-' }
]

const siNo = [
	{ label: 'si', value: 'si' },
	{ label: 'no', value: 'no' }
]

const rol = [
	{ label: 'Administrador', value: 'admin' },
	{ label: 'Usuario', value: 'user' }
]

const status = [
	{ label: 'Activa', value: 'active' },
	{ label: 'Inactiva', value: 'inactive' }
]
interface IComponent {
	open:boolean,
	filterState:any,
	handleFilterState:( e:any )=>void
}

const Component:FC<IComponent> = ( { open, filterState, handleFilterState } ) => {
	const classes = useStyles()
	const initialValuesString = useMemo( () => JSON.stringify( filterState ), [] )

	return (
		<Collapse in={ open }>
			<Paper sx={ { marginBottom: '12px' } }>
				<Box
					className={ classes.container }
				>
					<Box sx={ { display: 'flex', justifyContent: 'flex-start', alignItems: 'center', marginBottom: '20px' } }>
						<Typography variant='body1' color='primary'>Filtrar por:</Typography>
						<Box sx={ { flexGrow: 1 } } />
						{JSON.stringify( filterState ) !== initialValuesString && <Button variant='outlined' size='small' onClick={ () => handleFilterState( { target: { name: 'resetFilter' } } ) }>restablecer</Button>}
					</Box>
					<Grid container spacing={ 2 }>
						<Grid item xs={ 6 } md={ 3 }>
							<TextField select variant='outlined' size='small' name='status' label='Estado' fullWidth value={ filterState.status } onChange={ handleFilterState }>
								<MenuItem value={ '' }>Ninguno</MenuItem>
								{
									status.map( ( option ) => (
										<MenuItem key={ option.value } value={ option.value }>
											{option.label}
										</MenuItem>
									) )
								}
							</TextField>
						</Grid>
						<Grid item xs={ 6 } md={ 3 }>
							<TextField select variant='outlined' size='small' name='seguro' label='Seguro' fullWidth value={ filterState.seguro } onChange={ handleFilterState }>
								<MenuItem value={ '' }>Ninguno</MenuItem>
								{
									siNo.map( ( option ) => (
										<MenuItem key={ option.value } value={ option.value }>
											{option.label}
										</MenuItem>
									) )
								}
							</TextField>
						</Grid>
						<Grid item xs={ 6 } md={ 3 }>
							<TextField select variant='outlined' size='small' name='sangre' label='Tipo sangre' fullWidth value={ filterState.sangre } onChange={ handleFilterState }>
								<MenuItem value={ '' }>Ninguno</MenuItem>
								{
									tiposSangre.map( ( option ) => (
										<MenuItem key={ option.value } value={ option.value }>
											{option.label}
										</MenuItem>
									) )
								}
							</TextField>
						</Grid>
						<Grid item xs={ 6 } md={ 3 }>
							<TextField select variant='outlined' size='small' name='rol' label='Rol' fullWidth value={ filterState.rol } onChange={ handleFilterState }>
								<MenuItem value={ '' }>Ninguno</MenuItem>
								{
									rol.map( ( option ) => (
										<MenuItem key={ option.value } value={ option.value }>
											{option.label}
										</MenuItem>
									) )
								}
							</TextField>
						</Grid>
						<Grid item xs={ 6 } md={ 3 }>
							<Autocomplete
								value={ filterState.nacionalidad }
								onChange={ ( e, value ) => handleFilterState( { target: { name: 'nacionalidad', value: value } } ) }
								fullWidth
								noOptionsText='Sin resultado'
								size='small'
								options={ countries }
								renderInput={
									( params ) => (
										<TextField { ...params }
											label='País'
											autoComplete='off'
											size='small'
											variant="outlined"
											name='nacionalidad'
										/>
									)
								}
							/>
						</Grid>
					</Grid>
				</Box>
			</Paper>
		</Collapse>
	)
}

export default Component
