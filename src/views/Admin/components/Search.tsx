import { FC, useState, useCallback } from 'react'
import { Autocomplete, TextField } from '@material-ui/core'
import { debounce } from 'lodash'

import { apiAdmin } from '~/axios'

interface IComponent {
	url:string,
	q:string,
	handleQ:( e:any, val:any )=>void,
	filterState:any,
}

const Component:FC<IComponent> = ( { url, q, handleQ, filterState } ) => {
	const [isLoadingSearch, setIsLoadingSearch] = useState( false )
	const [searchOptions, setSearchOptions] = useState( [] )

	const debounceSearch = useCallback( debounce( async ( value ) => {
		setIsLoadingSearch( true )
		try {
			const { data } = await apiAdmin.get( url, {
				params: {
					$q: value,
					$limit: 5,
					...filterState
				}
			} )
			setSearchOptions( data.data )
		} catch ( error ) {
			console.log( error )
		}
	}, 800 ), [filterState] )

	const handleInputChange = ( e:any, value:any ) => {
		debounceSearch( value )
	}

	return (
		<>
			<Autocomplete
				value={ q }
				onChange={ handleQ }
				fullWidth
				noOptionsText='Sin resultado'
				size='small'
				options={ searchOptions }
				getOptionSelected={
					( option, value ) => {
						return ( option._id === value._id )
					}
				}
				getOptionLabel={
					( option:any ) => {
						return option.nombres + ' ' + option.apellidos + ' ' + option.cedula
					}
				}
				onInputChange={ handleInputChange }
				renderInput={
					( params ) => (
						<TextField { ...params }
							label='Buscar'
							autoComplete='off'
							size='small'
							variant="outlined"
							name='nacionalidad'
						/>
					)
				}
			/>
		</>
	)
}

export default Component
