import { FC, useMemo } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Collapse, Paper, TextField, Grid, Box, Typography, MenuItem, Button } from '@material-ui/core'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	container: {
		padding: '16px 16px'
	}
} ) )

const fields = [
	{ label: 'Nombres', value: 'nombres' },
	{ label: 'Apellidos', value: 'apellidos' },
	{ label: 'Cédula', value: 'cedula' },
	{ label: 'Correo', value: 'correo' },
	// { label: 'Teléfono', value: 'telefono' },
	{ label: 'Fecha de nacimiento', value: 'nacimiento' },
	{ label: 'Tipo sangre', value: 'sangre' },
	{ label: 'País', value: 'nacionalidad' },
	{ label: 'Posee seguro', value: 'seguro' },
	// { label: 'Contacto de emergencia', value: 'nombreemergencia' },
	// { label: 'Teléfono de emergencia', value: 'telefonoemergencia' },
	{ label: 'Estado', value: 'status' },
	{ label: 'Fecha de ingreso', value: 'createdAt' }
]

const directions = [
	{ label: 'Ascendente', value: '+' },
	{ label: 'Descendente', value: '-' }
]

interface IComponent {
	open:boolean,
	sortState:any,
	handleSortState:( e:any )=>void
}

const Component:FC<IComponent> = ( { open, sortState, handleSortState } ) => {
	const classes = useStyles()
	const initialValuesString = useMemo( () => JSON.stringify( sortState ), [] )
	return (
		<Collapse in={ open }>
			<Paper sx={ { marginBottom: '12px' } }>
				<Box
					className={ classes.container }
				>
					<Box sx={ { display: 'flex', justifyContent: 'flex-start', alignItems: 'center', marginBottom: '20px' } }>
						<Typography variant='body1' color='primary'>Ordenar por:</Typography>
						<Box sx={ { flexGrow: 1 } } />
						{JSON.stringify( sortState ) !== initialValuesString && <Button variant='outlined' size='small' onClick={ () => handleSortState( { target: { name: 'resetsort' } } ) }>restablecer</Button>}
					</Box>
					<Grid container spacing={ 2 }>
						<Grid item xs={ 6 } md={ 3 }>
							<TextField select variant='outlined' size='small' name='field' label='Campo' fullWidth value={ sortState.field } onChange={ handleSortState }>
								{
									fields.map( ( option ) => (
										<MenuItem key={ option.value } value={ option.value }>
											{option.label}
										</MenuItem>
									) )
								}
							</TextField>
						</Grid>
						<Grid item xs={ 6 } md={ 3 }>
							<TextField select variant='outlined' size='small' name='direction' label='Dirección' fullWidth value={ sortState.direction } onChange={ handleSortState }>
								{
									directions.map( ( option ) => (
										<MenuItem key={ option.value } value={ option.value }>
											{option.label}
										</MenuItem>
									) )
								}
							</TextField>
						</Grid>
					</Grid>
				</Box>
			</Paper>
		</Collapse>
	)
}

export default Component
