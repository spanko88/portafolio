import { FC, Fragment } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Box, Collapse, Typography, Grid, Hidden, Avatar, Button, Paper, Table, TableContainer, TableBody, TableCell, TableHead, TablePagination, TableRow } from '@material-ui/core'
import moment from 'moment'
import { capitalize } from 'lodash'
import { useRouter } from 'next/router'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	root: {
		width: '100%',
		marginBottom: '20px'
		// overflow: 'hidden'
	},
	container: {
		// maxHeight: '62vh',
	},
	row: {
		cursor: 'pointer'
	},
	infoRow: {
		backgroundColor: '#f5f0f1'
	}
} ) )
interface Column {
	id:'nombres'|'apellidos'|'correo'|'telefono'|'cedula'|'nacionalidad'|'deportecual'|'nacimiento'|'sangre'|'direccion'|'seguro'|'nombreemergencia'|'telefonoemergencia'|'motivacion'|'palabraidentifica'|'createdAt'|'completeName';
	label:string;
	minWidth?:number;
	align?:'right';
	format?:( value:number )=>string;
}

const columns:Column[] = [
	{ id: 'completeName', label: 'Nombre', minWidth: 280 },
	{ id: 'cedula', label: 'Cédula', minWidth: 100 },
	{ id: 'correo', label: 'Correo', minWidth: 100 },
	{ id: 'telefono', label: 'Teléfono', minWidth: 100 }
]

const collapseInfo1 = [
	{ id: 'nacionalidad', label: 'País', minWidth: 100 },
	{ id: 'cedula', label: 'Cédula', minWidth: 100 },
	{ id: 'correo', label: 'Correo', minWidth: 100 },
	{ id: 'telefono', label: 'Teléfono', minWidth: 100 },
	{ id: 'direccion', label: 'Dirección', minWidth: 200 },
	{ id: 'nacimiento', label: 'Fecha de nacimiento', minWidth: 180 },
	{ id: 'sangre', label: 'Tipo de sangre', minWidth: 140 },
	{ id: 'deportecual', label: 'Deportes que practica', minWidth: 200 }
]

const collapseInfo2 = [

	{ id: 'seguro', label: 'Posee seguro', minWidth: 40 },
	{ id: 'nombreemergencia', label: 'Contacto de emergencia', minWidth: 180 },
	{ id: 'telefonoemergencia', label: 'Teléfono de emergencia', minWidth: 180 },
	{ id: 'createdAt', label: 'Fecha de Ingreso', minWidth: 120 },
	{ id: 'palabraidentifica', label: 'Palabra que la identifica', minWidth: 100 },
	{ id: 'status', label: 'Estado', minWidth: 100 },
	{ id: 'motivacion', label: 'Motivación', minWidth: 100 }
]

interface IComponent {
	data:Array<any>,
	limit:number,
	handleLimit:( e:any )=>void,
	page:number,
	handlePage:( e:any, page:number )=>void,
	totalData:number,
	isCollapseOpen:null|number,
	handleCollapse:( val:null|number )=>void,
}

const Component:FC<IComponent> = ( { data, limit, handleLimit, page, handlePage, totalData, isCollapseOpen, handleCollapse } ) => {
	const classes = useStyles()
	const router = useRouter()

	const handleEditClick = ( id:string ) => {
		router.push( '/admin/' + id )
	}

	return (
		<Paper
			className={ classes.root }
		>
			<TablePagination
				rowsPerPageOptions={ [5, 10, 25, 100, 1000] }
				component="div"
				count={ totalData }
				rowsPerPage={ limit }
				page={ page }
				onPageChange={ handlePage }
				onRowsPerPageChange={ handleLimit }
				labelRowsPerPage=''
				nextIconButtonProps={ { size: 'small' } }
				backIconButtonProps={ { size: 'small' } }
				labelDisplayedRows={
					( { from, to, count } ) => {
						return `${from}-${to} de ${count !== -1 ? count : `mas de ${to}`}`
					}
				}
			/>
			<TableContainer
				className={ classes.container }
			>
				<Table stickyHeader size='small'>
					<TableHead>
						<TableRow>
							<TableCell style={ { width: 50 } }>#</TableCell>
							{
								columns.map( ( column ) => {
									switch ( column.id ) {
										case 'completeName' :
											return (
												<TableCell key={ `header_${column.id}` } align={ column.align }>
													<Typography variant='h5'><strong>{column.label}</strong></Typography>
												</TableCell>
											)

										default :
											return (
												<Hidden key={ `columns_${column.id}` } mdDown>
													<TableCell key={ `header_${column.id}` } align={ column.align }>
														<Typography variant='h5'><strong>{column.label}</strong></Typography>
													</TableCell>
												</Hidden>

											)
									}
								} )
							}
						</TableRow>
					</TableHead>
					<TableBody>
						{
							data.map( ( row:any, i:number ) => {
								return (
									<Fragment key={ `row_${row._id}` }>
										<TableRow className={ classes.row } hover tabIndex={ -1 } onClick={ () => handleCollapse( i ) }>
											<TableCell>{page * limit + i + 1}</TableCell>
											{
												columns.map( ( column ) => {
													const value = row[column.id]
													switch ( column.id ) {
														case 'completeName' :
															return (
																<TableCell key={ `columns_${column.id}` } align={ column.align }>
																	<Box sx={ { display: 'flex', justifyContent: 'flex-start', alignItems: 'center' } }>
																		<Avatar sx={ { marginRight: 1 } } />
																		{row.nombres.split( ' ' ).map( item => capitalize( item ) ).join( ' ' )}
																		{' '}
																		{row.apellidos.split( ' ' ).map( item => capitalize( item ) ).join( ' ' )}
																	</Box>
																</TableCell>
															)
														case 'createdAt' :
															return (
																<TableCell key={ `columns_${column.id}` } align={ column.align }>
																	{moment( value ).format( 'MM/DD/YYYY' )}
																</TableCell>
															)

														default :
															return (
																<Hidden key={ `columns_${column.id}` } mdDown>
																	<TableCell align={ column.align }>
																		{value}
																	</TableCell>
																</Hidden>

															)
													}
												} )
											}
										</TableRow>
										<TableRow className={ classes.infoRow }>
											<TableCell style={ { paddingBottom: 0, paddingTop: 0 } } colSpan={ 6 }>
												<Collapse in={ isCollapseOpen === i } timeout="auto" unmountOnExit>
													<Box sx={ { margin: 1 } }>
														<Grid container spacing={ 1 }>
															{
																collapseInfo1.map( ( column ) => {
																	const value = row[column.id]
																	switch ( column.id ) {
																		case 'nacimiento' :
																			return (
																				<Grid item key={ `collapse_${column.id}` } xs={ 12 } sm={ 6 }>
																					<Box sx={ { display: 'flex' } }>
																						<Typography variant="body1"><strong>{column.label}: </strong></Typography>
																						<Typography sx={ { marginLeft: 1 } } variant="body1">{moment( value ).format( 'DD/MM/YYYY' )}</Typography>
																					</Box>
																				</Grid>
																			)
																		case 'createdAt' :
																			return (
																				<Grid item key={ `collapse_${column.id}` } xs={ 12 } sm={ 6 }>
																					<Box key={ `collapse_${column.id}` } sx={ { display: 'flex' } }>
																						<Typography variant="body1"><strong>{column.label}: </strong></Typography>
																						<Typography sx={ { marginLeft: 1 } } variant="body1">{moment( value ).format( 'DD/MM/YYYY' )}</Typography>
																					</Box>
																				</Grid>
																			)

																		default :
																			return (
																				<Grid item key={ `collapse_${column.id}` } xs={ 12 } sm={ 6 }>
																					<Box key={ `collapse_${column.id}` } sx={ { display: 'flex' } }>
																						<Typography variant="body1"><strong>{column.label}: </strong></Typography>
																						<Typography sx={ { marginLeft: 1 } } variant="body1">{value}</Typography>
																					</Box>
																				</Grid>
																			)
																	}
																} )
															}
															{
																collapseInfo2.map( ( column ) => {
																	const value = row[column.id]
																	switch ( column.id ) {
																		case 'nacimiento' :
																			return (
																				<Grid item key={ `collapse_${column.id}` } xs={ 12 } sm={ 6 }>
																					<Box sx={ { display: 'flex' } }>
																						<Typography variant="body1"><strong>{column.label}: </strong></Typography>
																						<Typography sx={ { marginLeft: 1 } } variant="body1">{moment( value ).format( 'DD/MM/YYYY' )}</Typography>
																					</Box>
																				</Grid>
																			)
																		case 'status' :
																			return (
																				<Grid item key={ `collapse_${column.id}` } xs={ 12 } sm={ 6 }>
																					<Box sx={ { display: 'flex' } }>
																						<Typography variant="body1"><strong>{column.label}: </strong></Typography>
																						<Typography sx={ { marginLeft: 1 } } variant="body1">{value === 'active' ? 'activa' : 'inactiva'}</Typography>
																					</Box>
																				</Grid>
																			)
																		case 'createdAt' :
																			return (
																				<Grid item key={ `collapse_${column.id}` } xs={ 12 } sm={ 6 }>
																					<Box key={ `collapse_${column.id}` } sx={ { display: 'flex' } }>
																						<Typography variant="body1"><strong>{column.label}: </strong></Typography>
																						<Typography sx={ { marginLeft: 1 } } variant="body1">{moment( value ).format( 'DD/MM/YYYY' )}</Typography>
																					</Box>
																				</Grid>
																			)

																		default :
																			return (
																				<Grid item key={ `collapse_${column.id}` } xs={ 12 } sm={ 6 }>
																					<Box key={ `collapse_${column.id}` } sx={ { display: 'flex' } }>
																						<Typography variant="body1"><strong>{column.label}: </strong></Typography>
																						<Typography sx={ { marginLeft: 1 } } variant="body1">{value}</Typography>
																					</Box>
																				</Grid>
																			)
																	}
																} )
															}
															<Box sx={ { display: 'flex', justifyContent: 'flex-end', width: '100%', padding: '16px 0 6px' } }>
																<Button size='small' variant='contained' color='primary' onClick={ ( e ) => handleEditClick( row._id ) }>
																	editar
																</Button>
																{/* <Button sx={ { marginLeft: 1 } } size='small' variant='outlined' color='primary' onClick={ handleDeleteClick }>
																	eliminar
																</Button> */}
															</Box>
														</Grid>
													</Box>
												</Collapse>
											</TableCell>
										</TableRow>
									</Fragment>
								)
							} )
						}
					</TableBody>
				</Table>
			</TableContainer>
			<TablePagination
				rowsPerPageOptions={ [5, 10, 25, 100, 1000] }
				component="div"
				count={ totalData }
				rowsPerPage={ limit }
				page={ page }
				onPageChange={ handlePage }
				onRowsPerPageChange={ handleLimit }
				labelRowsPerPage=''
				nextIconButtonProps={ { size: 'small' } }
				backIconButtonProps={ { size: 'small' } }
				labelDisplayedRows={
					( { from, to, count } ) => {
						return `${from}-${to} de ${count !== -1 ? count : `mas de ${to}`}`
					}
				}
			/>
		</Paper>
	)
}

export default Component
