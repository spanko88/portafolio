import { useEffect, useState } from 'react'
import { Box, IconButton, Tooltip } from '@material-ui/core'
import { apiAdmin, cancelToken } from '~/axios'
import Table from './components/Table'
import Search from './components/Search'
import Filter from './components/Filter'
import Sort from './components/Sort'
import { useToasts } from 'react-toast-notifications'

import FilterIcon from '@material-ui/icons/FilterList'
import SortIcon from '@material-ui/icons/Sort'
import DownloadIcon from '@material-ui/icons/ArrowDownward'
import downloadJsonArrayToExcel from '~/utils/downloadJsonArray'

const initialFilterState = {
	status: '',
	seguro: '',
	sangre: '',
	rol: '',
	nacionalidad: null
}

const initialSortState = {
	field: 'createdAt',
	direction: '-'
}

const Component = ( ) => {
	const { addToast } = useToasts()
	const [data, setData] = useState( null )
	const [limit, setLimit] = useState( 100 )
	const [page, setPage] = useState( 0 )
	const [q, setQ] = useState( null )
	const [isFilterOpen, setIsFilterOpen] = useState( false )
	const [filterState, setFilterState] = useState( initialFilterState )
	const [isSortOpen, setIsSortOpen] = useState( false )
	const [sortState, setSortState] = useState( initialSortState )
	const [isCollapseOpen, setIsCollapseOpen] = useState( null )

	const handleLimit = ( e:any ) => {
		setLimit( e.target.value )
		setPage( 0 )
		setIsCollapseOpen( null )
	}

	const handlePage = ( e:any, page:number ) => {
		setPage( page )
		setIsCollapseOpen( null )
	}

	const handleQ = ( e:any, value:any ) => {
		setQ( value )
		setIsCollapseOpen( null )
	}

	const handleFilterClick = () => {
		setIsFilterOpen( state => !state )
	}

	const handleFilterState = ( e:any ) => {
		if ( e.target.name === 'resetFilter' ) {
			setFilterState( initialFilterState )
		} else {
			const { name, value } = e.target
			setFilterState( state => ( { ...state, [name]: value } ) )
		}
		setPage( 0 )
		setIsCollapseOpen( null )
	}

	const handleSortState = ( e:any ) => {
		if ( e.target.name === 'resetsort' ) {
			setSortState( initialSortState )
		} else {
			const { name, value } = e.target
			setSortState( state => ( { ...state, [name]: value } ) )
		}
		setPage( 0 )
		setIsCollapseOpen( null )
	}

	const handleSortClick = () => {
		setIsSortOpen( state => !state )
	}

	const handleCollapse = ( val:number|null ) => {
		if ( val === isCollapseOpen ) return setIsCollapseOpen( null )
		return setIsCollapseOpen( val )
	}

	const handleDownloadClick = () => {
		downloadJsonArrayToExcel( data.data, 'Warmis' )
	}

	useEffect( () => {
		const source = cancelToken.source()
		const getdata = async () => {
			try {
				const { data } = await apiAdmin.get( '/admin', {
					params: {
						$limit: limit,
						$page: page + 1,
						$sort: sortState.direction + sortState.field,
						$q: q?.nombres ?? '',
						...filterState
					},
					cancelToken: source.token
				} )
				setData( data )
			} catch ( error ) {
				console.log( error )
				if ( error?.response?.data?.message ) {
					addToast( error?.response?.data?.message, { appearance: 'error' } )
				} else {
					console.log( error.message )
					addToast( 'Existe un problema en tu conexión', { appearance: 'error' } )
				}
			}
		}
		getdata()

		return () => {
			source.cancel()
		}
	}, [limit, page, q, filterState, sortState] )

	return (
		<div>
			<Box sx={ { display: 'flex', alignItems: 'center', marginBottom: '6px' } }>
				<Search url={ '/admin' } filterState={ filterState } q={ q } handleQ={ handleQ } />
				<Box sx={ { flexGrow: 1 } } />
				<Tooltip title='Ordenar por'>
					<IconButton onClick={ handleSortClick }>
						<SortIcon />
					</IconButton>
				</Tooltip>
				<Tooltip title='Filtrar por'>
					<IconButton onClick={ handleFilterClick }>
						<FilterIcon />
					</IconButton>
				</Tooltip>
				<Tooltip title='Descargar'>
					<IconButton onClick={ handleDownloadClick }>
						<DownloadIcon />
					</IconButton>
				</Tooltip>
			</Box>
			<Filter open={ isFilterOpen } filterState={ filterState } handleFilterState={ handleFilterState } />
			<Sort open={ isSortOpen } sortState={ sortState } handleSortState={ handleSortState } />
			{
				data && <Table
					data={ data.data }
					limit={ limit }
					handleLimit={ handleLimit }
					page={ page }
					handlePage={ handlePage }
					totalData={ data.total }
					isCollapseOpen={ isCollapseOpen }
					handleCollapse={ handleCollapse }
				/>
			}
		</div>
	)
}

export default Component
