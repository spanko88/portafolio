import { useState } from 'react'
import { Form, Formik } from 'formik'
import { Button, Grid, Divider, Box, Typography, Paper } from '@material-ui/core'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'
import axios from 'axios'
import { useToasts } from 'react-toast-notifications'

import MuiFormikInput from '~/components/Form/MuiFormikInput'
import MuiFormikSelect from '~/components/Form/MuiFormikSelect'
import MuiFormikRadio from '~/components/Form/MuiFormikRadio'
import MuiFormikCheckbox from '~/components/Form/MuiFormikCheckbox'
import MuiFormikAC from '~/components/Form/MuiFormikAC'

import Loading from '~/components/Loading'
import sleep from '~/utils/sleep'
import countries from '~/utils/countries'

const tiposSangre = [
	{ label: 'O RH+', value: 'O RH+' },
	{ label: 'O RH-', value: 'O RH-' },
	{ label: 'A RH+', value: 'A RH+' },
	{ label: 'A RH-', value: 'A RH-' },
	{ label: 'B RH+', value: 'B RH+' },
	{ label: 'B RH-', value: 'B RH-' },
	{ label: 'AB RH+', value: 'AB RH+' },
	{ label: 'AB RH-', value: 'AB RH-' }
]

const siNo = [
	{ label: 'si', value: 'si' },
	{ label: 'no', value: 'no' }
]

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	paper: {
		padding: theme.spacing( 3 ),
		marginTop: theme.spacing( 3 )
	},
	divider: {
		margin: '12px 0'
	},
	button: {
		margin: '0 10px'
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
		alignItems: 'center'
	},
	click: {
		cursor: 'pointer'
	},
	bankInfo: {
		backgroundColor: '#d6c5cb',
		padding: '6px'
	}
} ) )

const validationSchema = Yup.object().shape( {
	apellidos: Yup.string().trim().lowercase().matches( /^[a-záéíóúñü]{1,}[a-záéíóúñü ]+$/gi, 'Apellidos no válidos' ).required( 'Campo requerido' ),
	nombres: Yup.string().trim().lowercase().matches( /^[a-záéíóúñü]{1,}[a-záéíóúñü ]+$/gi, 'Nombres no válidos' ).required( 'Campo requerido' ),
	cedula: Yup.string().trim().matches( /^[0-9]{10,}$/, 'Cédula no válida' ).required( 'Campo requerido' ),
	nacimiento: Yup.string().trim().required( 'Campo requerido' ),
	nacionalidad: Yup.string().typeError( 'Elige una opción' ),
	correo: Yup.string().trim().lowercase().matches( /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'Correo no válido' ).required( 'Campo requerido' ),
	direccion: Yup.string().trim().lowercase().required( 'Campo requerido' ),
	telefono: Yup.string().trim().matches( /^\d{6,}$/, 'Teléfono no válido' ).required( 'Campo requerido' ),
	sangre: Yup.string().trim().required( 'Campo requerido' ),
	seguro: Yup.string().trim().required( 'Campo requerido' ),
	nombreemergencia: Yup.string().trim().lowercase().matches( /^[a-záéíóúñü]{1,}[a-záéíóúñü ]+$/gi, 'Nombre no válido' ).required( 'Campo requerido' ),
	telefonoemergencia: Yup.string().trim().matches( /^\d{6,}$/, 'Teléfono no válido' ).required( 'Campo requerido' ),
	deporte: Yup.string().trim().required( 'Campo requerido' ),
	deportecual: Yup.string().trim().lowercase(),
	motivacion: Yup.string().trim().lowercase(),
	palabraidentifica: Yup.string().trim().lowercase().matches( /(^[a-záéíóúñü]{2,}[a-záéíóúñü]+$|^$)/gi, 'Una palabra' ),
	acepto: Yup.bool().oneOf( [true], 'Campo requerido' )
} )

const initialValues = {
	apellidos: '',
	nombres: '',
	cedula: '',
	nacimiento: '2000-01-01',
	correo: '',
	direccion: '',
	telefono: '',
	nacionalidad: null,
	sangre: '',
	seguro: 'no',
	nombreemergencia: '',
	telefonoemergencia: '',
	deporte: 'no',
	deportecual: '',
	motivacion: '',
	palabraidentifica: '',
	acepto: false
}

const AdminForm = () => {
	const classes = useStyles()
	const [isLoading, setIsLoading] = useState( false )
	const dispatch = useDispatch()
	const { addToast } = useToasts()

	const handleClose = () => setIsLoading( false )

	const onSubmit = async ( data, { setSubmitting, resetForm } ) => {
		setSubmitting( true )
		setIsLoading( true )
		await sleep( 3000 )
		const validatedData = await validationSchema.validate( data, { abortEarly: false, stripUnknown: true } )
		data = { ...validatedData }
		try {
			await axios.post( '/api', data )
			// console.log( resp )
			dispatch( { type: 'SUCCESS', payload: `${validatedData.nombres.toUpperCase()} ${validatedData.apellidos.toUpperCase()}` } )
		} catch ( error ) {
			if ( error?.response?.data ) {
				console.log( error?.response?.data?.message )
				addToast( error?.response?.data?.message, { appearance: 'error' } )
			} else {
				console.log( error.message )
				addToast( 'Existe un problema en tu conexión', { appearance: 'error' } )
			}
			setSubmitting( false )
			setIsLoading( false )
			dispatch( { type: 'ERROR', payload: null } )
		}
	}

	return (
		<Formik enableReinitialize validateOnMount initialValues={ initialValues } validationSchema={ validationSchema } onSubmit={ onSubmit }>
			{
				( { isSubmitting, dirty, errors, isValid, submitCount, values } ) => {
					return (
						<Form>
							<Paper className={ classes.paper }>
								<Grid container spacing={ 1 }>
									<Grid item xs={ 12 } md={ 6 }>
										<MuiFormikInput label='Apellidos' name='apellidos' />
									</Grid>
									<Grid item xs={ 12 } md={ 6 }>
										<MuiFormikInput label='Nombres' name='nombres' />
									</Grid>
									<Grid item xs={ 12 } md={ 3 }>
										<MuiFormikInput label='Cédula' name='cedula' />
									</Grid>
									<Grid item xs={ 12 } md={ 3 }>
										<MuiFormikInput label='Fecha de nacimiento' name='nacimiento' type='date' />
									</Grid>
									<Grid item xs={ 12 } md={ 3 }>
										<MuiFormikSelect label='Tipo de sangre' name='sangre' items={ tiposSangre } />
									</Grid>
									<Grid item xs={ 12 } md={ 3 }>
										<MuiFormikAC label='País de origen' name='nacionalidad' options={ countries } />
									</Grid>
									<Grid item xs={ 12 } md={ 4 }>
										<MuiFormikInput label='Correo' name='correo' />
									</Grid>
									<Grid item xs={ 12 } md={ 2 }>
										<MuiFormikInput label='Teléfono' name='telefono' />
									</Grid>
									<Grid item xs={ 12 } md={ 6 }>
										<MuiFormikInput label='Dirección' name='direccion' />
									</Grid>
									<Grid item xs={ 12 }>
										<Typography><strong>¿Dispones de seguro contra accidentes?</strong></Typography>
										<MuiFormikRadio label='' name='seguro' items={ siNo } />
									</Grid>
									<Grid item xs={ 12 }>
										<Typography sx={ { marginBottom: '12px', textAlign: 'justify' } }><strong>Contacto de emergencia</strong></Typography>
										<MuiFormikInput label='Nombre' name='nombreemergencia' />
									</Grid>
									<Grid item xs={ 12 }>
										<MuiFormikInput label='Teléfono' name='telefonoemergencia' />
									</Grid>
									<Grid item xs={ 12 }>
										<Typography sx={ { textAlign: 'justify' } }><strong>¿Tienes experiencia en deportes de aventura: montañismo, escalada, trail, ciclismo, etc.?</strong></Typography>
										<MuiFormikRadio label='' name='deporte' items={ siNo } />
										{
											values.deporte === 'si' && ( <MuiFormikInput label='Deporte' name='deportecual' /> )
										}
									</Grid>
									<Grid item xs={ 12 }>
										<Typography sx={ { marginBottom: '12px', textAlign: 'justify' } }><strong>Compártenos tu motivación para unirte a Yanasacha Warmis</strong></Typography>
										<MuiFormikInput label='Motivación' name='motivacion' multiline={ true } />
									</Grid>
									<Grid item xs={ 12 }>
										<Typography sx={ { marginBottom: '12px', textAlign: 'justify' } }><strong>Palabra que te identifica como persona</strong></Typography>
										<MuiFormikInput label='Palabra' name='palabraidentifica' />
									</Grid>
									<Grid item xs={ 12 }>
										<Typography sx={ { textAlign: 'justify', marginTop: '6px' } }>Con tu inscripción aceptas la contribución de $5 Anuales. Yanasacha Warmis es un grupo sin fines de lucro, por lo que todo lo recaudado siempre es para financiar la compra de equipo de uso general para salidas de grupo</Typography>
										<Box sx={ { display: 'flex', justifyContent: 'center', alignItems: 'center' } }>
											<MuiFormikCheckbox label='' name='acepto' />
											<Typography color='primary' sx={ { textAlign: 'justify', marginTop: '6px' } }>Acepto inscribirme</Typography>
										</Box>
										<Box sx={ { display: 'flex', justifyContent: 'center', alignItems: 'center' } }>
											<Typography className={ classes.bankInfo } sx={ { textAlign: 'justify', marginTop: '6px' } }><strong>Deposito o transferencia:</strong> BANCO PICHINCHA, cuenta 5045837100, a nombre de TATIANA VEGA, cédula 1712229770</Typography>
										</Box>
									</Grid>
								</Grid>

								<Divider className={ classes.divider } />
								<div className={ classes.buttons }>
									<Box sx={ { marginLeft: '12px' } }>
										<Button type="submit" color="primary" disabled={ isSubmitting || !dirty || !isValid } size="medium" variant="contained">
											enviar
										</Button>
									</Box>
								</div>
								<Loading isLoading={ isLoading } handleClose={ handleClose } />
							</Paper>
						</Form>
					)
				}
			}
		</Formik>
	)
}

export default AdminForm
