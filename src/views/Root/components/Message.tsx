import { FC, ReactNode } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Typography, Paper } from '@material-ui/core'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	paper: {
		padding: theme.spacing( 3 )
	},
	title: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: '12px'
	},
	text: {
		textAlign: 'justify',
		marginTop: '10px'
	}
} ) )

interface IComponent {
	children?:ReactNode
}

const Component:FC<IComponent> = ( { children } ) => {
	const classes = useStyles()
	return (
		<Paper className={ classes.paper }>
			<Typography variant='h2' color='primary' className={ classes.title }>Formulario de inscripción</Typography>
			<Typography variant='body1'>Bienvenida a nuestra familia!</Typography>
			<Typography variant='body1'className={ classes.text }>Somos un grupo de mujeres que amamos la Pachamama, la respetamos y la valoramos, caminamos juntas entre valles y montañas, promovemos el senderismo, el montañismo inclusivo y participativo como una actividad más allá del ámbito deportivo, como un medio de desarrollo personal y solidaridad colectiva. Somos un grupo de deportistas amateur que avanzan hacia sus objetivos a través de una planificación y programación, todo esto mediante la autogestión y autofinanciamiento</Typography>
		</Paper>
	)
}

export default Component
