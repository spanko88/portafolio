import { FC, ReactNode, useEffect } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Paper, Typography } from '@material-ui/core'
import { useToasts } from 'react-toast-notifications'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	paper: {
		padding: theme.spacing( 3 )
	},
	text: {
		textAlign: 'justify',
		marginTop: '10px'
	}
} ) )

interface IComponent {
	children?:ReactNode,
	name?:string
}

const Component:FC<IComponent> = ( { children, name } ) => {
	const classes = useStyles()
	const { addToast } = useToasts()

	useEffect( () => {
		addToast( 'Bienvenido', { appearance: 'success' } )
	}, [] )

	return (
		<Paper className={ classes.paper }>
			<Typography variant='h2'>Bienvenida <strong>{name}</strong> </Typography>
			<Typography variant='body1'className={ classes.text }>Muy pronto estaremos en contacto contigo</Typography>
		</Paper>
	)
}

export default Component
