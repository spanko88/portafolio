import { FC, ReactNode } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { useSelector } from 'react-redux'

import Header from './components/Header'
import Form from './components/Form'
import Message from './components/Message'
import Success from './components/Success'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	container: {
		padding: `${theme.spacing( 3 )} ${theme.spacing( 6 )} ${theme.spacing( 6 )}`,
		[theme.breakpoints.down( 'sm' )]: {
			padding: theme.spacing( 1 )
		}
	}
} ) )

interface IComponent {
	children?:ReactNode
}

const Component:FC<IComponent> = ( { children } ) => {
	const classes = useStyles()
	const info = useSelector( ( state:any ) => state )
	return (
		<>
			<Header />
			<div className={ classes.container }>
				{
					info.user.name
						? (
							<Success name={ info.user.name } />
						)
						: (
							<>
								<Message />
								<Form />
							</>
						)
				}
			</div>
		</>
	)
}

export default Component
