import Link from 'next/link'
import Form from './components/Form'
import Status from './components/Status'
import Aportes from './components/Aportes'
import { Box, Button } from '@material-ui/core'

const Component = ( { initialValues } ) => {
	return (
		<>
			<Box sx={ { display: 'flex', justifyContent: 'flex-end', alignItems: 'center' } }>
				<Link href='/admin'>
					<Button variant='outlined'>regresar</Button>
				</Link>
			</Box>
			<Form initialValues={ initialValues } />
			<Status initialValues={ initialValues } />
			<Aportes initialValues={ initialValues } />
		</>
	)
}

export default Component
