import { useState } from 'react'
import { useRouter } from 'next/router'
import { Form, Formik } from 'formik'
import { Button, Grid, Divider, Box, Paper, Typography } from '@material-ui/core'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import * as Yup from 'yup'
import { useToasts } from 'react-toast-notifications'

import MuiFormikSelect from '~/components/Form/MuiFormikSelect'

import Loading from '~/components/Loading'
import sleep from '~/utils/sleep'
import { apiAdmin } from '~/axios'

const roles = [
	{ label: 'Administrador', value: 'admin' },
	{ label: 'Usuario', value: 'user' }
]

const status = [
	{ label: 'Activa', value: 'active' },
	{ label: 'Inactiva', value: 'inactive' }
]

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	paper: {
		padding: theme.spacing( 3 ),
		marginTop: theme.spacing( 3 )
	},
	divider: {
		margin: '12px 0'
	},
	button: {
		margin: '0 10px'
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
		alignItems: 'center'
	},
	click: {
		cursor: 'pointer'
	},
	bankInfo: {
		backgroundColor: '#d6c5cb',
		padding: '6px'
	}
} ) )

const validationSchema = Yup.object().shape( {
	rol: Yup.string().required( 'Campo requerido' ),
	status: Yup.string().required( 'Campo requerido' )
} )

const Component = ( { initialValues } ) => {
	const classes = useStyles()
	const router = useRouter()
	const [isLoading, setIsLoading] = useState( false )
	const { addToast } = useToasts()

	const handleClose = () => setIsLoading( false )

	const onSubmit = async ( formData, { setSubmitting, resetForm } ) => {
		setSubmitting( true )
		setIsLoading( true )
		await sleep( 3000 )
		const validatedData = await validationSchema.validate( formData, { abortEarly: false, stripUnknown: true } )
		try {
			await apiAdmin.patch( `/admin/${formData._id}`, validatedData )
			router.reload()
		} catch ( error ) {
			if ( error?.response?.data ) {
				console.log( error )
				addToast( error?.response?.data?.message, { appearance: 'error' } )
			} else {
				console.log( error.message )
				addToast( 'Existe un problema en tu conexión', { appearance: 'error' } )
			}
			setSubmitting( false )
			setIsLoading( false )
		}
	}

	return (
		<Formik enableReinitialize validateOnMount initialValues={ initialValues } validationSchema={ validationSchema } onSubmit={ onSubmit }>
			{
				( { isSubmitting, dirty, isValid, values } ) => {
					return (
						<Form>
							<Paper className={ classes.paper }>
								<Typography sx={ { marginBottom: 3 } } variant='h4' color='primary'>Cuenta</Typography>
								<Grid container spacing={ 1 }>
									<Grid item xs={ 12 } md={ 6 }>
										<MuiFormikSelect label='Rol' name='rol' items={ roles } />
									</Grid>
									<Grid item xs={ 12 } md={ 6 }>
										<MuiFormikSelect label='Estado' name='status' items={ status } />
									</Grid>
								</Grid>

								<Divider className={ classes.divider } />
								<div className={ classes.buttons }>
									<Box sx={ { marginLeft: '12px' } }>
										<Button type='reset' disabled={ isSubmitting || !dirty } size="small" variant="outlined" className={ classes.button } onClick={ handleClose }>
											cancelar
										</Button>
										<Button type="submit" color='primary' disabled={ isSubmitting || !dirty } size="small" variant="contained">
											guardar
										</Button>
									</Box>
								</div>
								<Loading isLoading={ isLoading } handleClose={ handleClose } />
							</Paper>
						</Form>
					)
				}
			}
		</Formik>
	)
}

export default Component
