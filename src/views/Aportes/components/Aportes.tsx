import { useState } from 'react'
import { useRouter } from 'next/router'
import { Form, Formik } from 'formik'
import { Button, Grid, Divider, Box, Paper, Typography, TableRow, TableCell, Table, TableContainer, TableHead, TableBody } from '@material-ui/core'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import * as Yup from 'yup'
import { useToasts } from 'react-toast-notifications'
import moment from 'moment'

import { MuiFormikInput, MuiFormikNumeric } from '~/components/Form'

import Loading from '~/components/Loading'
import sleep from '~/utils/sleep'
import { apiAdmin } from '~/axios'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	paper: {
		padding: theme.spacing( 3 ),
		marginTop: theme.spacing( 3 )
	},
	divider: {
		margin: '12px 0'
	},
	button: {
		margin: '0 10px'
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
		alignItems: 'center'
	},
	click: {
		cursor: 'pointer'
	},
	bankInfo: {
		backgroundColor: '#d6c5cb',
		padding: '6px'
	}
} ) )

const initials = {
	description: '',
	value: '',
	confirm: false
}

const validationSchema = Yup.object().shape( {
	description: Yup.string().trim().lowercase().required( 'Campo requerido' ),
	value: Yup.string().trim().matches( /^\d*$/, 'Ingresa un valor' ).required( 'Campo requerido' ),
	confirm: Yup.boolean()
} )

const Component = ( { initialValues } ) => {
	const classes = useStyles()
	const router = useRouter()
	const [isLoading, setIsLoading] = useState( false )
	const { addToast } = useToasts()

	const handleClose = () => setIsLoading( false )

	const handleConfirmation = async ( i:number ) => {
		setIsLoading( true )
		const aportes = []
		aportes.push( ...initialValues.aportes )
		aportes[i] = { ...aportes[i], confirm: true }
		try {
			await apiAdmin.patch( `/admin/${initialValues._id}`, { aportes } )
			router.reload()
		} catch ( error ) {
			if ( error?.response?.data ) {
				console.log( error )
				addToast( error?.response?.data?.message, { appearance: 'error' } )
			} else {
				console.log( error.message )
				addToast( 'Existe un problema en tu conexión', { appearance: 'error' } )
			}
			setIsLoading( false )
		}
	}

	const onSubmit = async ( formData, { setSubmitting, resetForm } ) => {
		setSubmitting( true )
		setIsLoading( true )
		await sleep( 3000 )
		const validatedData = await validationSchema.validate( formData, { abortEarly: false, stripUnknown: true } )
		const aportes = []
		aportes.push( ...initialValues.aportes )
		aportes.push( { ...validatedData, created: new Date().toISOString() } )
		try {
			await apiAdmin.patch( `/admin/${initialValues._id}`, { aportes } )
			router.reload()
		} catch ( error ) {
			if ( error?.response?.data ) {
				console.log( error )
				addToast( error?.response?.data?.message, { appearance: 'error' } )
			} else {
				console.log( error.message )
				addToast( 'Existe un problema en tu conexión', { appearance: 'error' } )
			}
			setSubmitting( false )
			setIsLoading( false )
		}
	}

	return (
		<Formik enableReinitialize validateOnMount initialValues={ initials } validationSchema={ validationSchema } onSubmit={ onSubmit }>
			{
				( { isSubmitting, dirty, isValid, values, setValues } ) => {
					return (
						<Form>
							<Paper className={ classes.paper }>
								<Typography sx={ { marginBottom: 3 } } variant='h4' color='primary'>Aportes Económicos</Typography>
								<Grid container spacing={ 1 }>
									<Grid item xs={ 12 } md={ 6 }>
										<MuiFormikInput label='Descripción' name='description' />
									</Grid>
									<Grid item xs={ 12 } md={ 6 }>
										<MuiFormikNumeric type='number' label='Valor' name='value' min={ 0 } max={ 100 } />
									</Grid>
								</Grid>
								<Box>
									<Button
										type='button'
										variant='contained'
										color='primary'
										size='small'
										onClick={ ( e ) => setValues( { description: 'subscripción', value: 5 } ) }
									>
										Suscripción: $5
									</Button>
								</Box>

								<Divider className={ classes.divider } />
								<div className={ classes.buttons }>
									<Box sx={ { marginLeft: '12px' } }>
										<Button type='reset' disabled={ isSubmitting || !dirty } size="small" variant="outlined" className={ classes.button } onClick={ handleClose }>
											cancelar
										</Button>
										<Button type="submit" color='primary' disabled={ isSubmitting || !dirty } size="small" variant="contained">
											guardar
										</Button>
									</Box>
								</div>
								<TableContainer>
									<Table size='small'>
										<TableHead>
											<TableRow>
												<TableCell>
													Descripción
												</TableCell>
												<TableCell>
													Valor
												</TableCell>
												<TableCell>
													Fecha Ingreso
												</TableCell>
												<TableCell>
													Confirmación pago
												</TableCell>
											</TableRow>
										</TableHead>
										<TableBody>
											{
												initialValues.aportes && initialValues.aportes.map( ( item:typeof initials & {created:Date}, i:number ) => (
													<TableRow key={ `aporte_${Math.random() * 10000 + item.description}` }>
														<TableCell>
															{item.description}
														</TableCell>
														<TableCell>
															{item.value}
														</TableCell>
														<TableCell>
															{moment( item.created ).format( 'DD/MM/YYYY HH:mm:ss' )}
														</TableCell>
														<TableCell>
															{
																item.confirm
																	? <Button
																		disabled
																		type='button'
																		variant='outlined'
																		size='small'
																	>
																		Verificado
																	</Button>
																	: <Button
																		type='button'
																		variant='contained'
																		size='small'
																		onClick={ ( e ) => handleConfirmation( i ) }
																	>
																		verificar
																	</Button>
															}
														</TableCell>
													</TableRow>
												) )
											}
										</TableBody>
									</Table>
								</TableContainer>
							</Paper>
							<Loading isLoading={ isLoading } handleClose={ handleClose } />
						</Form>
					)
				}
			}
		</Formik>
	)
}

export default Component
