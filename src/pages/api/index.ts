/* eslint-disable no-case-declarations */
import { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'
import User from '~/models/user'
import reconnectDB from '~/middlewares/reconnectDB'

const api = async ( req:NextApiRequest, res:NextApiResponse ) => {
	// console.log( req.body )
	await reconnectDB( )

	switch ( req.method ) {
		case 'POST' :
			const data = Object.entries( req.body ).map( ( keyValue, iKey ) => {
				return `${keyValue[0]} = ${keyValue[1]}`
			} )
			const message = 'Nueva inscripción' + '\n' + '\n' + data.join( '\n' )
			const user:any = new User( req.body )
			try {
				await user.save()
				await axios.get( process.env.TELEGRAM_URL, {
					params: {
						chat_id: -1001268837122,
						text: message
					}
				} )
				return res.status( 201 ).json( { status: 'saved' } )
			} catch ( error ) {
				console.log( error )
				if ( error.code === 11000 ) {
					const errorArrayMessages = Object.entries( error.keyValue ).map( ( keyVal, iKeyVal ) => {
						return `El usuario con ${keyVal[0]} ${keyVal[1]} ya existe.`
					} )
					return res.status( 400 ).json( { status: 'error', message: errorArrayMessages[0] } )
				}
				return res.status( 500 ).json( { status: 'error', message: error.message } )
			}

		default :
			return res.status( 422 ).json( { status: 'error', message: 'El servicio no existe' } )
	}
}

export default api
