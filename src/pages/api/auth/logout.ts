import { NextApiRequest, NextApiResponse } from 'next'
import cookieServer from 'cookie'

const api = async ( req:NextApiRequest, res:NextApiResponse ) => {
	res.setHeader( 'set-cookie', cookieServer.serialize( 'authorization', '', {
		httpOnly: true,
		secure: true,
		expires: new Date( 0 ),
		sameSite: 'strict',
		path: '/'
	} ) )

	return res.status( 200 ).json( { status: 'ok' } )
}

export default api
