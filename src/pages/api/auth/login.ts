import { NextApiRequest, NextApiResponse } from 'next'
import { sign } from 'jsonwebtoken'
import { compare } from 'bcryptjs'
import cookieServer from 'cookie'

import { reconnectBD } from '~/middlewares'
import User from '~/models/user'

const api = async ( req:NextApiRequest, res:NextApiResponse ) => {
	switch ( req.method ) {
		case 'POST' :
			try {
				// if correo or password keys not exist, response bad request
				const { correo, password } = req.body
				if ( !correo || !password ) return res.status( 400 ).json( { status: 'error', message: 'datos no válidos' } )

				// check if user exist adn has a password, otherwise response bad request
				await reconnectBD()
				const user:any = await User.findOne( { correo: correo } ).select( 'password rol' ).lean()
				if ( !user ) return res.status( 400 ).json( { status: 'error', message: 'correo y/o contraseña incorrectos' } )
				if ( !user.password ) return res.status( 400 ).json( { status: 'error', message: 'correo y/o contraseña incorrectos' } )

				// check if user hashed password match, otherwise response bad request
				const isSamePassword = await compare( password, user.password )
				if ( !isSamePassword ) return res.status( 400 ).json( { status: 'error', message: 'correo y/o contraseña incorrectos' } )

				// else generate token
				const token = sign( { _id: user._id }, process.env.JWT_KEY, { expiresIn: '7d' } )

				// cookie http only headers
				res.setHeader( 'set-cookie', cookieServer.serialize( 'authorization', token, {
					httpOnly: false,
					secure: false,
					maxAge: 60 * 60 * 24 * 7,
					sameSite: 'strict',
					path: '/'
				} ) )
				return res.status( 201 ).json( { status: 'ok', token: token, redirect: user.rol === 'admin' ? '/admin?welcome=true' : '/user?welcome=true' } )
			} catch ( error ) {
				console.log( error )
				return res.status( 500 ).json( { status: 'error', message: 'Hubo un problema, intentalo mas tarde' } )
			}

		default :
			return res.status( 422 ).json( { status: 'error', message: 'El servicio no existe' } )
	}
}

export default api
