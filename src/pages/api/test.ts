import { NextApiRequest, NextApiResponse } from 'next'
import reconnectDB from '~/middlewares/reconnectDB'
import User from '~/models/user'

const api = async ( req:NextApiRequest, res:NextApiResponse ) => {
	await reconnectDB()
	switch ( req.method ) {
		case 'GET' :
			try {
				const users:any = await User.smartQuery( req.query )
				return res.status( 200 ).json( {
					status: 'ok',
					page: parseInt( String( req.query.$page ) || '1' ),
					total: await User.smartCount( req.query ),
					data: users
				} )
			} catch ( error ) {
				return res.status( 500 ).json( { status: 'error' } )
			}

		default :
			return res.status( 422 ).json( { status: 'error', message: 'El servicio no existe' } )
	}
}

export default api
