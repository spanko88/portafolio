/* eslint-disable no-case-declarations */
import { NextApiRequest, NextApiResponse } from 'next'
import reconnectDB from '~/middlewares/reconnectDB'
import authorizationMiddleware from '~/middlewares/authorization'
import authenticationMiddleware from '~/middlewares/authentication'
import { getAll as getUsers } from '~/controllers/users.controllers'
const api = async ( req:NextApiRequest, res:NextApiResponse ) => {
	// console.log( req.body )
	await reconnectDB()

	switch ( req.method ) {
		case 'GET' :
			try {
				const userId = await authorizationMiddleware( req )
				await authenticationMiddleware( userId, 'admin' )
				const data = await getUsers( req )
				return res.status( 200 ).json( { status: 'ok', ...data } )
			} catch ( error ) {
				console.log( error )
				if ( error.status ) return res.status( error.status ).json( { status: 'error', message: error.message } )
				return res.status( 500 ).json( { status: 'error' } )
			}
		default :
			res.status( 422 ).send( 'request method not supported' )
	}
}

export default api
