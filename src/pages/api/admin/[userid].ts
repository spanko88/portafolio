/* eslint-disable no-case-declarations */
import { NextApiRequest, NextApiResponse } from 'next'
import reconnectDB from '~/middlewares/reconnectDB'
import authorization from '~/middlewares/authorization'
import authentication from '~/middlewares/authentication'
import { updateOne as updateOneUser } from '~/controllers/users.controllers'
const api = async ( req:NextApiRequest, res:NextApiResponse ) => {
	const { userid } = req.query
	await reconnectDB()

	switch ( req.method ) {
		case 'PATCH' :
			try {
				const id = await authorization( req )
				await authentication( id, 'admin' )
				const data = await updateOneUser( req, userid )
				return res.status( 200 ).json( { status: 'saved', data } )
			} catch ( error ) {
				console.log( error )
				if ( error.status ) return res.status( error.status ).json( { status: 'error', message: error.message } )
				return res.status( 500 ).json( { status: 'error' } )
			}
		default :
			res.status( 422 ).send( 'request method not supported' )
	}
}

export default api
