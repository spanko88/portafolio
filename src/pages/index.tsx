/* eslint-disable react/no-unescaped-entities */
import Layout from '~/Layouts/Admin'
import { Box, Typography, Card, CardMedia, CardContent, CardActions, Button } from '@material-ui/core'
import { motion } from 'framer-motion'

const Component = ( ) => {
	return (
		<Layout>
			<motion.div
				initial={ { opacity: 0 } }
				animate={ { opacity: 1 } }
				transition={ { duration: 2 } }
			>
				<Box sx={ { border: 'solid 1px #fff', padding: '20px', marginBottom: '10px', height: '140px', display: 'flex', alignItems: 'center' } }>
					<Typography variant='h2' color='textPrimary' sx={ { textAlign: 'justify' } }>
						Hi!, my name is Erik, I'm a full stack developer MERN - Nextjs.
					</Typography>
				</Box>
			</motion.div>
			<Typography variant='h4' color='primary' sx={ { margin: '16px 0 8px' } }>
				Some projects
			</Typography>
			<Box sx={ { paddingBottom: '20px', display: 'grid', gridTemplateColumns: 'repeat(auto-fill,minmax(250px,1fr))', gap: '10px' } }>

				<Card>
					<CardMedia
						style={ { height: '180px' } }
						image="/static/images/sismedin.png"
						title="sismedin s.a."
					/>
					<CardActions>
						<Button size="small">
							<a href={ 'https://www.sismedin.com/' } rel="noreferrer" target="_blank">View</a>
						</Button>
					</CardActions>
					<CardContent>
						<Typography gutterBottom variant="h5" component="div">
							Sismedin CMS
						</Typography>
						<Typography variant="body2" color="inherit" sx={ { textAlign: 'justify' } }>
							CMS developed for SISMEDIN S.A., it uses Nextjs, Express, UI Design, gcloud-storage, JWT authentication.
						</Typography>
					</CardContent>
				</Card>

				<Card>
					<CardMedia
						style={ { height: '180px' } }
						image="/static/images/warmis.png"
						title="yanasacha warmis"
					/>
					<CardActions>
						<Button size="small">
							<a href={ 'https://warmis.vercel.app/' } rel="noreferrer" target="_blank">View</a>
						</Button>
					</CardActions>
					<CardContent>
						<Typography gutterBottom variant="h5" component="div">
							Yanasacha Warmis CMS
						</Typography>
						<Typography variant="body2" color="inherit" sx={ { textAlign: 'justify' } }>
							This web application allows you to manage the database of registered users, assign permissions, and control payments.
							This application uses Material-UI, mongoDB Atlas, telegram-API for notifications, nextjs (SSR)
						</Typography>
					</CardContent>
				</Card>

				<Card>
					<CardMedia
						style={ { height: '180px' } }
						image="/static/images/master-speaker-skills.png"
						title="master speaker skills"
					/>
					<CardActions>
						<Button size="small">
							<a href={ 'https://rueda-competencias.vercel.app/' } rel="noreferrer" target="_blank">View</a>
						</Button>
					</CardActions>
					<CardContent>
						<Typography gutterBottom variant="h5" component="div">
							Master Speaker Skills Test
						</Typography>
						<Typography variant="body2" color="inherit" sx={ { textAlign: 'justify' } }>
							Interactive test which shows your progress with a polar graph, at the end of the test a pdf document is generated with the details.
							This application uses chartjs, nextjs, mongoDB, responsive web design.
						</Typography>
					</CardContent>
				</Card>

				<Card>
					<CardMedia
						style={ { height: '180px' } }
						image="/static/images/dashboard.png"
						title="master speaker skills"
					/>
					<CardActions>
						<Button size="small">
							<a href={ 'https://interfaz-pro.vercel.app/home' } rel="noreferrer" target="_blank">View</a>
						</Button>
					</CardActions>
					<CardContent>
						<Typography gutterBottom variant="h5" component="div">
							Dashboard
						</Typography>
						<Typography variant="body2" color="inherit" sx={ { textAlign: 'justify' } }>
							This app uses light/dark mode, also some cool animations in the drawer, it's made with material-UI, framer-motion
						</Typography>
					</CardContent>
				</Card>

			</Box>
		</Layout>
	)
}

export default Component
