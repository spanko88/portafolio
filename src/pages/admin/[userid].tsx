import Head from 'next/head'
import { GetServerSideProps } from 'next'
import Aportes from '~/views/Aportes'
import AdminLayout from '~/Layouts/Admin'

import { authentication, authorization, reconnectBD } from '~/middlewares'
import { getOne } from '~/controllers/users.controllers'

const Component = ( { userObject } ) => {
	const user = JSON.parse( userObject )
	return (
		<AdminLayout>
			<Head>
				<title>Warmi {user.cedula}</title>
			</Head>
			<Aportes initialValues={ user } />
		</AdminLayout>
	)
}

const errorResponse = ( redirectUrl:string, message:string|null = '', messageType:string|null = '' ) => {
	return {
		props: {},
		redirect: {
			permanent: false,
			destination: `${redirectUrl}?message=${message}&messageType=${messageType}`
		}
	}
}

export const getServerSideProps:GetServerSideProps = async ( { req, res, params } ) => {
	const { userid } = params
	try {
		const id = await authorization( req )
		await reconnectBD()
		await authentication( id, 'admin' )
		const data = await getOne( req, userid )

		return {
			props: {
				userObject: JSON.stringify( data )
			}
		}
	} catch ( error ) {
		console.log( error.status, error.name, error.message )
		if ( error.name === 'UnauthorizedError' ) return errorResponse( '/generic', error.message, 'info' )
		else if ( error.name === 'JsonWebTokenError' ) return errorResponse( '/generic', 'no autorizado', 'warning' )
		else if ( error.type === 'MongooseError' ) return errorResponse( '/generic', 'Opss.. intentalo mas tarde', 'error' )
		return errorResponse( '/generic', error.name + ': ' + error.message, 'info' )
	}
}

export default Component
