import { useEffect } from 'react'
import Head from 'next/head'
import { GetServerSideProps } from 'next'
import Admin from '~/views/Admin'
import AdminLayout from '~/Layouts/Admin'
import { useToasts } from 'react-toast-notifications'
import { capitalize } from 'lodash'

import { authentication, authorization, reconnectBD } from '~/middlewares'

const Component = ( { welcome, nombres } ) => {
	const { addToast } = useToasts()

	useEffect( () => {
		welcome && addToast( 'Bienvenida ' + capitalize( nombres.split( ' ' )[0] ), { appearance: 'success' } )
	}, [welcome] )

	return (
		<AdminLayout>
			<Head>
				<title>Yanasacha Warmis - administrador</title>
			</Head>
			<Admin />
		</AdminLayout>
	)
}

const errorResponse = ( redirectUrl:string, message:string|null = '', messageType:string|null = '' ) => {
	return {
		props: {},
		redirect: {
			permanent: false,
			destination: `${redirectUrl}?message=${message}&messageType=${messageType}`
		}
	}
}

export const getServerSideProps:GetServerSideProps = async ( { req, res, query } ) => {
	try {
		const id = await authorization( req )
		await reconnectBD()
		const user = await authentication( id, 'admin' )
		return {
			props: {
				nombres: user.nombres,
				apellidos: user.apellidos,
				welcome: query.welcome || ''
			}
		}
	} catch ( error ) {
		console.log( error.status, error.name, error.message )
		if ( error.name === 'UnauthorizedError' ) return errorResponse( '/generic', error.message, 'info' )
		else if ( error.name === 'JsonWebTokenError' ) return errorResponse( '/generic', 'no autorizado', 'warning' )
		else if ( error.type === 'MongooseError' ) return errorResponse( '/generic', 'Opss.. intentalo mas tarde', 'error' )
		return errorResponse( '/generic', error.name + error.message, 'info' )
	}
}

export default Component
