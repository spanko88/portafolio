/* eslint-disable react/no-unescaped-entities */
import Layout from '~/Layouts/Admin'
import { Box, Typography } from '@material-ui/core'
import { motion } from 'framer-motion'

const Component = ( ) => {
	return (
		<Layout>
			<motion.div
				initial={ { opacity: 0 } }
				animate={ { opacity: 1 } }
				transition={ { duration: 2 } }
			>
				<Box sx={ { border: 'solid 1px #fff', padding: '20px', marginBottom: '10px', height: '100px', display: 'flex', alignItems: 'center' } }>
					<Typography variant='h2' color='textPrimary' sx={ { textAlign: 'justify' } }>
						I'm working on it
					</Typography>
				</Box>
			</motion.div>
		</Layout>
	)
}

export default Component
