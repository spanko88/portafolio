import { Box, Typography } from '@material-ui/core'
import { GetServerSideProps } from 'next'

const Component = ( { message } ) => {
	return (
		<Box sx={ { width: '100vw', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' } }>
			<Typography variant='h2'>
				{message}
			</Typography>
		</Box>
	)
}

export default Component

export const getServerSideProps:GetServerSideProps = async ( { req, res,query } ) => {
	console.log( 'not found' )
	const { message } = query
	return {
		props: {
			message: message || ''
		}
	}
}
