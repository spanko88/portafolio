import { useEffect } from 'react'
import { AppProps } from 'next/app'
import { ThemeProvider } from '@material-ui/core/styles'
import { CacheProvider } from '@emotion/react'
import createCache from '@emotion/cache'
import GlobalStyles from 'src/components/GlobalStyles'
import Head from 'next/head'

import { ToastProvider } from 'react-toast-notifications'

import theme from '~/theme/index'

import { wrapper } from '../store/store'
import { useStore } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import LocalizationProvider from '@material-ui/lab/LocalizationProvider'
import 'moment/locale/es-us'
import moment from 'moment'
import MomentAdapter from '@material-ui/lab/AdapterMoment'
import { LinearProgress } from '@material-ui/core'

moment.locale( 'es-us' ) // it is required to select default locale manually

export const cache = createCache( { key: 'css', prepend: true } )

function MyApp ( props:AppProps ) {
	const { Component, pageProps } = props
	const store:any = useStore( )
	useEffect( () => {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector( '#jss-server-side' )
		if ( jssStyles ) {
			jssStyles.parentElement!.removeChild( jssStyles )
		}
	}, [] )

	return (
		<CacheProvider value={ cache }>
			<ThemeProvider theme={ theme }>
				<PersistGate persistor={ store.__persistor } loading={ <LinearProgress color='primary' /> }>
					<LocalizationProvider dateLibInstance={ moment } dateAdapter={ MomentAdapter } locale={ 'es-us' }>
						<ToastProvider autoDismiss={ true } autoDismissTimeout={ 6000 } placement='bottom-right'>
							<Head>
								{/* PWA primary color */}
								<title>Erik Rodriguez - portafolio</title>
								<meta name="viewport" content="initial-scale=1, width=device-width, initial-scale=1" key='title' />
								<meta name="description" content= "Mi portafolio, descripción personal, aptitudes, mi trabajo" />
								<meta name="robots" content= "index, follow" />
								<meta httpEquiv="Content-type" content="text/html; charset=utf-8" />
								<meta name="keywords" content="programador,desarrolador,web,react, quito, ecuador" />
								<meta name="author" content="erik rodriguez" />
								<meta name="copyright" content="Propietario del copyright" />
								<meta name="theme-color" content={ theme.palette.primary.main } />
								<meta httpEquiv="expires" content="43200" />
							</Head>
							{/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
							<GlobalStyles />
							<Component { ...pageProps } />
						</ToastProvider>
					</LocalizationProvider>
				</PersistGate>
			</ThemeProvider>
		</CacheProvider>
	)
}

export default wrapper.withRedux( MyApp )
