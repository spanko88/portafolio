import Layout from '~/Layouts/Admin'
import { Box, Button } from '@material-ui/core'
import { motion } from 'framer-motion'

const Component = ( ) => {
	return (
		<Layout>
			<motion.div
				initial={ { opacity: 0 } }
				animate={ { opacity: 1 } }
				transition={ { duration: 2 } }
			>
				<Box sx={ { border: 'solid 1px #fff', padding: '20px', marginBottom: '10px', display: 'flex', alignItems: 'center' } }>
					<Button size="small">
						<a href={ 'https://gitlab.com/spanko88' } rel="noreferrer" target="_blank">Gitlab profile</a>
					</Button>
				</Box>
				<Box sx={ { border: 'solid 1px #fff', padding: '20px', marginBottom: '10px', display: 'flex', alignItems: 'center' } }>
					<Button size="small">
						<a href={ '/static/resume.pdf' } rel="noreferrer" target="_blank">Resume (CV)</a>
					</Button>
				</Box>
			</motion.div>
		</Layout>
	)
}

export default Component
