import { useEffect, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import { Paper, Container, Button, Typography, Grid } from '@material-ui/core'

import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import { useToasts } from 'react-toast-notifications'

import Loading from '~/components/Loading'
import sleep from '~/utils/sleep'
import MuiFormikInput from '~/components/Form/MuiFormikInput'
import { GetServerSideProps } from 'next'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	container: {
		height: '100vh',
		width: '100vw',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	paper: {
		padding: theme.spacing( 4 )
	},
	submitButton: {
		marginTop: theme.spacing( 2 ),
		marginBottom: theme.spacing( 2 ),
		width: '100%'
	}
} ) )

const validationSchema = Yup.object().shape( {
	correo: Yup.string().trim().lowercase().matches( /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'Correo no válido' ).required( 'Campo requerido' ),
	password: Yup.string().required( 'Contraseña requerida' )
} )

interface IFormValues {
	correo:string,
	password:string
}

const initialValues:IFormValues = {
	correo: '',
	password: ''
}

const Component = ( { message, messageType } ) => {
	const classes = useStyles()
	const router = useRouter()
	const { addToast } = useToasts()
	const [isLoading, setIsLoading] = useState( false )

	const handleClose = () => setIsLoading( false )

	const onSubmit = async ( formData, { setSubmitting, resetForm } ) => {
		setSubmitting( true )
		setIsLoading( true )
		await sleep( 1000 )
		const validatedData = await validationSchema.validate( formData, { abortEarly: false, stripUnknown: true } )
		formData = { ...validatedData }
		try {
			const { data } = await axios.post( '/api/auth/login', formData )
			router.push( data.redirect )
		} catch ( error ) {
			console.log( error )
			if ( error?.response?.data?.message ) {
				addToast( error?.response?.data?.message, { appearance: 'error' } )
			} else {
				console.log( error.message )
				addToast( 'Existe un problema en tu conexión', { appearance: 'error' } )
			}
			setSubmitting( false )
			setIsLoading( false )
		}
	}

	useEffect( () => {
		message && addToast( message, { appearance: messageType } )
	}, [message] )

	return (
		<>
			<Head>
				<title>Yanasacha Warmis - iniciar sesión</title>
			</Head>
			{
				<div className={ classes.container }>
					<Container maxWidth='xs'>
						<Paper className={ classes.paper }>
							<Formik initialValues={ initialValues } validationSchema={ validationSchema } onSubmit={ onSubmit }>
								{
									( { isSubmitting, isValid, dirty } ) => (
										<Form>
											<Typography variant='h2' color='primary' sx={ { marginBottom: '30px' } }>Iniciar Sesión</Typography>
											<Grid container spacing={ 1 }>
												<Grid item xs={ 12 }>
													<MuiFormikInput label='Correo' name='correo' />
												</Grid>
												<Grid item xs={ 12 }>
													<MuiFormikInput label='Contraseña' name='password' type='password' />
												</Grid>
											</Grid>
											<Button className={ classes.submitButton } color="primary" disabled={ isSubmitting || !isValid || !dirty } fullWidth size='medium' type="submit" variant="contained">
												ingresar
											</Button>
										</Form>
									)
								}
							</Formik>
							<Loading isLoading={ isLoading } handleClose={ handleClose } />
						</Paper>
					</Container>
				</div>
			}
		</>
	)
}

export const getServerSideProps:GetServerSideProps = async ( { req, res, query } ) => {
	console.log( 'gssp login' )
	const { message, messageType } = query
	return {
		props: {
			message: message || '',
			messageType: messageType || ''
		}
	}
}

export default Component
