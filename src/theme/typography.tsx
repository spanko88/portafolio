export default {
	h1: {
		fontWeight: 500,
		fontSize: 35,
		fontFamily: 'Roboto',
		letterSpacing: '-0.24px'
	},
	h2: {
		fontWeight: 500,
		fontSize: 29,
		fontFamily: 'Roboto',
		letterSpacing: '-0.24px'
	},
	h3: {
		fontWeight: 500,
		fontSize: 24,
		fontFamily: 'Roboto',
		letterSpacing: '-0.06px'
	},
	h4: {
		fontWeight: 400,
		fontSize: 20,
		fontFamily: 'Roboto',
		letterSpacing: '-0.06px'
	},
	h5: {
		fontWeight: 300,
		fontSize: 16,
		fontFamily: 'Roboto',
		letterSpacing: '-0.05px'
	},
	h6: {
		fontWeight: 300,
		fontSize: 14,
		fontFamily: 'Roboto',
		letterSpacing: '-0.05px'
	},
	h7: {
		fontSize: '12px',
		fontFamily: 'Roboto',
		letterSpacing: '-0.05px'
	},
	subtitle1: {
		fontSize: '16px',
		letterSpacing: '-0.05px',
		lineHeight: '25px'
	},
	subtitle2: {
		fontWeight: 400,
		fontSize: '14px',
		letterSpacing: '-0.05px',
		lineHeight: '21px'
	},
	body1: {
		fontSize: '14px',
		letterSpacing: '-0.05px',
		lineHeight: '21px'
	},
	body2: {
		fontSize: '14px',
		letterSpacing: '-0.04px',
		lineHeight: '18px'
	},
	button: {
		fontSize: '14px'
	},
	caption: {
		fontSize: '11px',
		letterSpacing: '0.33px',
		lineHeight: '13px'
	}
}
