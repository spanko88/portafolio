import { createMuiTheme, responsiveFontSizes, colors } from '@material-ui/core'
import typography from './typography'

let theme = createMuiTheme(
	{
		palette: {
			secondary: {
				main: colors.red[900]
			},
			text: {
				primary: colors.blueGrey[900],
				secondary: colors.blueGrey[600]
			},
			error: {
				main: '#c9225b'
			}
		},
		typography
	}
)

theme = responsiveFontSizes( theme )

export default theme
