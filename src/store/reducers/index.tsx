import { combineReducers } from 'redux'
import { userReducer, IUser } from './authAdminReducer'

export interface IRootState {
	user:IUser;
}

export default combineReducers( {
	user: userReducer
} )
