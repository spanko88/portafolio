import * as t from '~/store/types'
export interface IUser {
	name:string | null,
	error:boolean,
	errorMsg:string
}

const initialState:IUser = {
	name: null,
	error: false,
	errorMsg: ''
}

export const userReducer = ( state = initialState, { type, payload } ) => {
	switch ( type ) {
		case t.SUCCESS :
			return {
				...state,
				name: payload,
				error: false
			}

		case t.ERROR :
			return {
				...state,
				error: true,
				errorMsg: 'Intentelo mas tarde'
			}

		default :
			return state
	}
}
