import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createWrapper } from 'next-redux-wrapper'
import { persistStore, persistReducer } from 'redux-persist'
import { createWhitelistFilter } from 'redux-persist-transform-filter'

import storage from 'redux-persist/lib/storage'
import rootReducer from './reducers'

const initialState = {}
const middleware = [thunk]

const makeStore:any = ( { isServer } ) => {
	if ( isServer ) {
		// If it's on server side, create a store
		return createStore( rootReducer, initialState, composeWithDevTools( applyMiddleware( ...middleware ) ) )
	} else {
		// If it's on client side, create a store which will persist
		const persistConfig = {
			key: 'nextjs',
			storage,
			transforms: [
				// createBlacklistFilter∆( 'user' )
				// createWhitelistFilter( 'user' )
				createWhitelistFilter( 'user', ['none'] )
			] // if needed, use a safer storage
		}

		const persistedReducer = persistReducer( persistConfig, rootReducer ) // Create a new reducer with our existing reducer

		const store:any = createStore(
			persistedReducer,
			composeWithDevTools( applyMiddleware( ...middleware )
			)
		) // Creating the store again
		// This creates a persistor object & push that persisted object to .__persistor, so that we can avail the persistability feature
		store.__persistor = persistStore( store )
		return store
	}
}

// Export the wrapper & wrap the pages/_app.js with this wrapper only
export const wrapper = createWrapper( makeStore )
