import mongoose from 'mongoose'

const reconnectDB = async ( ) => {
	// console.log( 'mongoose conenctions', mongoose.connections.length )
	if ( mongoose.connections[0].readyState ) {
		// Use current db connection
		return
	}

	console.log( 'new db connection' )
	// Use new db connection
	mongoose.set( 'toJSON', { virtuals: true } )
	await mongoose.connect( process.env.MONGODB_URL, {
		useUnifiedTopology: true,
		useFindAndModify: true,
		useCreateIndex: true,
		useNewUrlParser: true
	} )
}

export default reconnectDB
