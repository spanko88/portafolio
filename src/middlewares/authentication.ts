import User from '~/models/user'
import createHttpError from 'http-errors'

// rol representa el valor con el cual la key rol tiene que hacer match
const middleware = async ( id:string, rol:string ) => {
	// si el usuario no existe ó el rol no coincide, bota una excepción
	const user:any = await User.findById( id ).select( '-password' ).lean()
	if ( !user ) throw new createHttpError.NotFound( 'no existe usuario' )
	if ( user.rol !== rol ) throw new createHttpError.Forbidden( 'no autorizado' )
	return user
}

export default middleware
