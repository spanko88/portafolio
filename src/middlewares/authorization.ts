import { verify } from 'jsonwebtoken'
import { NextApiRequest } from 'next'
import createHttpError from 'http-errors'

const middleware = async ( req:NextApiRequest| any ) => {
	// si no existe cookie authorization bota una excepción
	const { authorization } = req.cookies
	if ( !authorization ) throw new createHttpError.Unauthorized( 'Expir%C3%B3 Sesi%C3%B3n' )

	// verifica JWToken si no lanza una excepción, si todo esta bien, devuelve el parametro: _id del token
	const decodedToken:any = verify( authorization, process.env.JWT_KEY, { ignoreExpiration: true } )
	return decodedToken._id
}

export default middleware
