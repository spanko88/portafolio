import { apiAdmin } from '~/axios'

const fetcher = async ( url:string ) => {
	const { data } = await apiAdmin.get( url )
	return data
}

export default fetcher
