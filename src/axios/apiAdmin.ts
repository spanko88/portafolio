import axios from 'axios'
// import axiosRetry from 'axios-retry'

const api = axios.create( {
	baseURL: '/api'
	// timeout: 1000
} )

api.interceptors.request.use( ( config ) => {
	config.headers.Authorization = `Bearer ${localStorage.getItem( 'authorization' )}`
	return config
}, ( error ) => {
	console.log( error )
	return Promise.reject( error )
} )

api.interceptors.response.use( ( response ) => {
	return response
}, ( error ) => {
	// console.log( Object.entries( error ) )
	return Promise.reject( error )
} )

// axiosRetry( api, {
// retries: 1
// retryDelay: axiosRetry.exponentialDelay
// shouldResetTimeout: true,
// retryDelay: ( count ) => { return count * 2000 }
// } )
export default api
