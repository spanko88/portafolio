import { FC, ReactNode } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { AppBar, Toolbar, IconButton, Typography } from '@material-ui/core'
import { useRouter } from 'next/router'
import axios from 'axios'
import Link from 'next/link'

import LogoutIcon from '@material-ui/icons/ExitToApp'
interface IComponent {
	children?:ReactNode
}

const Component:FC<IComponent> = ( { children } ) => {
	const classes = useStyles()
	const router = useRouter()
	// const { data, error, isValidating } = useSwr( '/login/admin', fetcher )
	// console.log( data, error, isValidating )

	const handleLogout = async () => {
		try {
			await axios.get( '/api/auth/logout' )
			localStorage.clear()
			router.push( '/login' )
		} catch ( error ) {
			console.log( error )
		}
	}

	return (
		<div className={ classes.container }>
			<AppBar>
				<Toolbar variant='dense' className={ classes.toolbar }>
					<Typography variant='h4'>
						Portafolio
					</Typography>
					<div style={ { flexGrow: 1 } } />
					<Link href='/'>
						<a className={ classes.link }>Home</a>
					</Link>
					{/* <Link href='/blog'>
						<a className={ classes.link }>Blog</a>
					</Link> */}
					<Link href='/contact'>
						<a className={ classes.link }>Contact</a>
					</Link>
				</Toolbar>
			</AppBar>
			<div className={ classes.content }>
				{children}
			</div>
		</div>
	)
}

export default Component

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	container: {
		display: 'flex'
	},
	toolbar: {
		display: 'flex',
		justifyContent: 'flex-end',
		alignItems: 'center'
	},
	content: {
		marginTop: '48px',
		height: 'calc(100vh - 48px)',
		width: '100%',
		padding: theme.spacing( 3 )
	},
	link: {
		marginLeft: '6px',
		textDecoration: 'none',
		color: '#fff',
		border: 'solid 1px transparent',
		padding: '4px',
		transition: 'border 0.6s',
		'&:hover': {
			border: 'solid 1px #fff'
		}
	}
} ) )
