# Proyecto inscripción Warmis
Este proyecto usa:
+ Formik - Yup
+ Framer-motion
+ Material-UI
+ Resposive design
+ Api rest
+ Mongodb-mongoose

## Variables de entorno
+ JWT_KEY = \<**string**\>
+ MONGODB_URL = mongodb+srv://\<**account**\>:\<**password**\>@\<**address**\>/\<**db**\>?retryWrites=true&w=majority
+ TELEGRAM_URL = https://api.telegram.org/bot\<**token**\>/sendMessage
## Todos:

- [ ] Agregar un footer con nuestra marca
- [ ] tabla users CRUD - ruta protegida
- [ ] Carrito de compras - cloud Storage
- [ ] dashboard admin - pagina protegida

- [x] Login view - login & server-side api